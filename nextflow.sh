#!/bin/bash
#SBATCH --job-name=EASEL
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=10G
#SBATCH --mail-user=

module load nextflow
module load singularity

nextflow run main.nf -profile singularity -params-file funaria.yaml

