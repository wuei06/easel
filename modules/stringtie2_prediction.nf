process ESThints_stringtie2 {
    publishDir "$params.outdir/05_hints/stringtie2",  mode: 'copy'
    label 'process_high'

    	conda (params.enable_conda ? "bioconda::gmap" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/gmap:2021.08.25--pl5321h67092d7_2"
    } else {
        container "quay.io/biocontainers/gmap:2021.08.25--pl5321h67092d7_2"
    }  

    input:
    path(gmapIndex)
    path(stringtie2transdecoderCDS)
    path(script)
     
    output:
    path "eggnog.estHints.gff", emit: stringtie2_est
    
    script:
    """
    gmap -D ${gmapIndex} -d gmap ${stringtie2transdecoderCDS} --nthreads=${task.cpus} --min-intronlength=30 --intronlength=500000 --trim-end-exons=20 -f 1 -n 0 > gmap_Eggnog.psl 
    cat gmap_Eggnog.psl | sort -n -k 16,16 | sort -s -k 14,14 | perl -ne '@f=split; print if (\$f[0]>=100)' | ${script}/blat2hints.pl --source=PB --nomult --ep_cutoff=20 --in=/dev/stdin --out=eggnog.estHints.gff
    """
}

process proteinHints_stringtie2 {
    publishDir "$params.outdir/05_hints/stringtie2",  mode: 'copy' 
    label 'process_high'

    	conda (params.enable_conda ? "bioconda::genomethreader" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/genomethreader:1.7.1--h87f3376_4"
    } else {
        container "quay.io/biocontainers/genomethreader:1.7.1--h87f3376_4"
    }  

    input:
    path(script)
    path(genome)
    path(stringtie2transdecoderPEP)
    path(bin)
    path(miniprot)
    val(N)
    val(outn)
    val(max_intron)
     
    output:
    path "eggnog.protHints.gff", emit: stringtie2_protein
    
    script:  
    if (params.aligner == 'gth')
    """
    ${script}/startAlign.pl --genome=${genome} --prot=${stringtie2transdecoderPEP} --prg=gth --CPU=${task.cpus}
    ${script}/align2hints.pl --in=align_gth/gth.concat.aln --out=eggnog.protHints.gff --prg=gth
    """
    else if (params.aligner == 'miniprot')
    """
    ${miniprot}/miniprot -N ${N} --outn=${outn} -G ${max_intron} -t ${task.cpus} ${genome} ${stringtie2transdecoderPEP} --gff-only > miniprot.gff
    ${script}/align2hints.pl --in=miniprot.gff --out=eggnog.protHints.gff --prg=gth
    """
}

process trainingSet_stringtie2 {
    label 'process_low'
    tag { id }

    	conda (params.enable_conda ? "bioconda::augustus" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/augustus:3.5.0--pl5321hf46c7bb_1"
    } else {
        container "quay.io/biocontainers/augustus:3.5.0--pl5321hf46c7bb_1"
    }  

    input:
    path(stringtie2Gmap)
    path(genome)
    path(AUGUSTUS_CONFIG_PATH)
    path(script)
    path(bin)
    val(species)
    val(test)
    val(train)
     
    output:
    path "*.gb", emit: gb
    
    script:
    """
    gffread ${stringtie2Gmap} -T -o gmap.stringtie2.gtf
    ${script}/computeFlankingRegion.pl gmap.stringtie2.gtf | tee calculate_flank.out
    flanking_DNA=\$(grep -A0 "flanking_DNA" calculate_flank.out | awk '{print \$5}')
    echo "\$flanking_DNA" >> calculate_flank.out
    ${script}/gff2gbSmallDNA.pl ${stringtie2Gmap} ${genome} \$flanking_DNA stringtie2Raw.gb
    ${script}/new_species.pl --species="$species"_stringtie2
    ${bin}/etraining --species="$species"_stringtie2 stringtie2Raw.gb 2> failed_genes.txt
    awk '{print \$7}' failed_genes.txt | sed s/://g > badTrainingGenes.lst
    ${script}/filterGenes.pl badTrainingGenes.lst stringtie2Raw.gb > stringtie2GoodGenes.gb
    grep -c "LOCUS" *.gb | tee calculate_flank.out
    ${script}/randomSplit.pl stringtie2GoodGenes.gb ${test}
    ${script}/randomSplit.pl stringtie2GoodGenes.gb.train ${train}
    rm stringtie2GoodGenes.gb.train
    rm stringtie2GoodGenes.gb.train.train
    mv stringtie2GoodGenes.gb.train.test train.gb
    mv stringtie2GoodGenes.gb.test test.gb
    grep -c "LOCUS" *.gb >> calculate_flank.out
    ${bin}/etraining --species="$species"_stringtie2 train.gb
    ${bin}/augustus --species="$species"_stringtie2 | tee stringtie2_untrained.out
    ${script}/optimize_augustus.pl --species="$species"_stringtie2 train.gb --cpus=${task.cpus} --kfold=12 --aug_exec_dir ${AUGUSTUS_CONFIG_PATH}
    ${bin}/etraining --species="$species"_stringtie2 train.gb
    ${bin}/augustus --species="$species"_stringtie2 test.gb | tee stringtie2_optimized.out

    """
}

process augustusEST_stringtie2 {
    label 'process_medium'
    tag { id }

    	conda (params.enable_conda ? "bioconda::augustus" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/augustus:3.5.0--pl5321hf46c7bb_1"
    } else {
        container "quay.io/biocontainers/augustus:3.5.0--pl5321hf46c7bb_1"
    }  

    input:
    path(gb)
    path(ESThints)
    path(script)
    path(configEST)
    path(AUGUSTUS_CONFIG_PATH)
    path(bin)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*stringtie2.ESTHints.gff3", emit: stringtie2_estHints
     
    script: 
    """
    ${bin}/augustus --species="$species"_stringtie2 ${genome_chunk} --extrinsicCfgFile=${configEST} --alternatives-from-evidence=true --hintsfile=${ESThints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_stringtie2.ESTHints.gff3
    """
}

process augustusProtein_stringtie2 {
    label 'process_medium'
    tag { id }

    	conda (params.enable_conda ? "bioconda::augustus" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/augustus:3.5.0--pl5321hf46c7bb_1"
    } else {
        container "quay.io/biocontainers/augustus:3.5.0--pl5321hf46c7bb_1"
    }  

    input:
    path(gb)
    path(proteinHints)
    path(script)
    path(configProtein)
    path(AUGUSTUS_CONFIG_PATH)
    path(bin)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*stringtie2.protHints.gff3", emit: stringtie2_proteinHints
    
    script:  
    """
    ${bin}/augustus --species="$species"_stringtie2 ${genome_chunk} --extrinsicCfgFile=${configProtein} --alternatives-from-evidence=true --hintsfile=${proteinHints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_stringtie2.protHints.gff3

    """
}
process combineEST_stringtie2 {
    publishDir "$params.outdir/06_predictions/stringtie2",  mode: 'copy'
    label 'process_medium'
    tag { id }

    	conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }

    input:
    path(estHints)
     
    output:
    path("est_stringtie2.gff"), emit: estGFF
      
    """
mkdir est
mv ${estHints} est
for f in est/*.gff3
do
    if wc -l \$f < 25; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
gff=\$(echo \$files)

agat_sp_merge_annotations.pl \$gff -o est_stringtie2.gff

    """
}

process combineProtein_stringtie2 {
    publishDir "$params.outdir/06_predictions/stringtie2",  mode: 'copy'
    label 'process_medium'
    tag { id }

    	conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }

    input:
    path(proteinHints)
     
    output:
    path("protein_stringtie2.gff"), emit: stringtie_proteinHints
      
    """
mkdir protein
mv ${proteinHints} protein
for f in protein/*.gff3
do
    if wc -l \$f < 25; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
gff=\$(echo \$files)

agat_sp_merge_annotations.pl \$gff -o protein_stringtie2.gff

    """
}

process combineAll_stringtie2 {
    publishDir "$params.outdir/06_predictions/stringtie2",  mode: 'copy'
    label 'process_medium'
    tag { id }

        conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }

    input:
    path(augustus_protein)
    path(augustus_est)
     
    output:
    path("stringtie2_unfiltered.gff"), emit: stringtie2_combined
      
    """

agat_sp_merge_annotations.pl -f ${augustus_protein} -f ${augustus_est} -o stringtie2_unfiltered.gff

    """
}