process mergeGFF {
    publishDir "$params.outdir/final_predictions",  mode: 'copy'
    label 'process_medium'

    	conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }

    input:
    path(stringtie2)
    path(psiclass)
    val(species)
     
    output:
    path("*unfiltered.gff"), emit: unfiltered
    
    """
    agat_sp_merge_annotations.pl -f ${stringtie2} -f ${psiclass} -o "$species"_unfiltered.gff

    """
}
process mergeGFF_orthodb {
    publishDir "$params.outdir/final_predictions",  mode: 'copy'
    label 'process_medium'

    	conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }

    input:
    path(stringtie2)
    path(psiclass)
    path(s_ortho)
    path(p_ortho)
    val(species)
     
    output:
    path("*unfiltered.gff"), emit: unfiltered_db
    
    """
    agat_sp_merge_annotations.pl -f ${stringtie2} -f ${psiclass} -f ${s_ortho} -f ${p_ortho} -o "$species"_unfiltered.gff

    """
}
process list_orthodb {
    label 'process_single'
    
    input:
    path(s_est)
    path(s_prot)
    path(s_trans)
    path(p_est)
    path(p_prot)
    path(p_trans)
    path(p_orthodb)
    path(s_orthodb)

    output:
    path("list.txt"), emit: gff
    path("list_2.txt"), emit: trans
      
    """
    readlink -f ${s_est} >> list.txt
    readlink -f ${s_prot} >> list.txt
    readlink -f ${p_est} >> list.txt
    readlink -f ${p_prot} >> list.txt
    readlink -f ${p_orthodb} >> list.txt
    readlink -f ${s_orthodb} >> list.txt

    readlink -f ${s_trans} >> list_2.txt
    readlink -f ${p_trans} >> list_2.txt
    """
}
process list {
    label 'process_single'
    
    input:
    path(s_est)
    path(s_prot)
    path(p_est)
    path(p_prot)

    output:
    path("list.txt"), emit: gff

    """
    readlink -f ${s_est} >> list.txt
    readlink -f ${s_prot} >> list.txt
    readlink -f ${p_est} >> list.txt
    readlink -f ${p_prot} >> list.txt

    """
}
