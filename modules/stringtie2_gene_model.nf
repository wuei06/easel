process stringtie2 {
        publishDir "$params.outdir/04_assembly/stringtie2/gtf",  mode: 'copy'	
        label 'process_medium' 
        tag { id }

    	conda (params.enable_conda ? "bioconda::stringtie=2.2.1" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/stringtie:2.2.1--hecb563c_2"
    } else {
        container "quay.io/biocontainers/stringtie:2.2.1--hecb563c_2"
    }

    	input:
        tuple val(id), path(bam)

    	output:
        path("*.gtf"), emit: stringtie2_gtf 

    	script:
    	""" 
        stringtie ${bam} -p ${task.cpus} -o ${id}.gtf
    	"""
}

process merge_stringtie2 {
	publishDir "$params.outdir/04_assembly/stringtie2",  mode: 'copy', pattern: '*fa'	
    publishDir "$params.outdir/04_assembly/stringtie2/gtf",  mode: 'copy', pattern: '*gtf'
    publishDir "$params.outdir/log",  mode: 'copy', pattern: '*txt'

    label 'process_medium' 
    tag { id }

    	conda (params.enable_conda ? "bioconda::stringtie=2.2.1" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/stringtie:2.2.1--hecb563c_2"
    } else {
        container "quay.io/biocontainers/stringtie:2.2.1--hecb563c_2"
    }

    	input:
    file(gtf)
	path(genome)
	
    	output:
    path("stringtie2.gtf"), emit: merged_gtf
	path("stringtie_transcripts.fa"), emit: fasta  
    path("log_stringtie2.txt"), emit: log_stringtie

    	script:
    	"""   
	stringtie --merge -p ${task.cpus} -o stringtie2.gtf *.gtf
	gffread -w stringtie_transcripts.fa -g ${genome} stringtie2.gtf

    transcripts=\$(echo | grep -c ">" stringtie_transcripts.fa)
    echo " " >> log_stringtie2.txt
    echo "##### StringTie2 Transcriptome Asssembly #####" >> log_stringtie2.txt
    echo "\$transcripts transcripts" >> log_stringtie2.txt
    	"""
}

process orf_stringtie2 {

    publishDir "$params.outdir/04_assembly/stringtie2/frameselection",  mode: 'copy'
    label 'process_medium'

    	conda (params.enable_conda ? "bioconda::transdecoder" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/transdecoder:5.5.0--pl5321hdfd78af_5"
    } else {
        container "quay.io/biocontainers/transdecoder:5.5.0--pl5321hdfd78af_5"
    }   

    input:
    path(stringtie2Transcriptome) 
     
    output:
    path "${stringtie2Transcriptome}.transdecoder_dir/longest_orfs.pep", emit:longOrfsPepFiles
    path "${stringtie2Transcriptome}.transdecoder_dir/", emit:LongOrfsDirFiles 
    path "${stringtie2Transcriptome}.transdecoder_dir.__checkpoints_longorfs/", emit:LongOrfsCheckpointsFiles
    path "*.cmds", emit:longOrfsRootCmds
      
    script:
    """
    TransDecoder.LongOrfs -t ${stringtie2Transcriptome}
    """
}

process eggnog_stringtie2 {
    publishDir "$params.outdir/04_assembly/stringtie2/frameselection",  mode: 'copy'
    label 'process_medium' 

    	conda (params.enable_conda ? "bioconda::eggnog-mapper" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/eggnog-mapper:2.1.9--pyhdfd78af_0"
    } else {
        container "quay.io/biocontainers/eggnog-mapper:2.1.9--pyhdfd78af_0"
    }   

    input:
    path(stringtiePep)    

    output:
    path "*.hits", emit:eggnogBlastp 
   
    script:
    """
    emapper.py -i ${stringtiePep} --no_annot -o eggnog.blastp -m diamond --cpu ${task.cpus}

    """
}

process predict_stringtie2 {
    publishDir "$params.outdir/04_assembly/stringtie2/frameselection",  mode: 'copy'
    label 'process_medium'
    
    	conda (params.enable_conda ? "bioconda::transdecoder" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/transdecoder:5.5.0--pl5321hdfd78af_5"
    } else {
        container "quay.io/biocontainers/transdecoder:5.5.0--pl5321hdfd78af_5"
    }   

    input:
    path(stringtie2TranscriptomePredict) 
    path(eggnogBlastp)
    path(directory)
     
    output:
    path "${stringtie2TranscriptomePredict}.transdecoder.pep", emit: transdecoderPEP
    path "${stringtie2TranscriptomePredict}.transdecoder.bed", emit: transdecoderBED
    path "${stringtie2TranscriptomePredict}.transdecoder.cds", emit: transdecoderCDS
    path "${stringtie2TranscriptomePredict}.transdecoder.gff3", emit: transdecoderGFF3

    script:
    """
    TransDecoder.Predict -t ${stringtie2TranscriptomePredict} --no_refine_starts --retain_blastp_hits ${eggnogBlastp}
    """
}

process genemodel_stringtie2 {
    label 'process_medium'

    	conda (params.enable_conda ? "bioconda::gmap" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/gmap:2021.08.25--pl5321h67092d7_2"
    } else {
        container "quay.io/biocontainers/gmap:2021.08.25--pl5321h67092d7_2"
    }      

    input:
    path(stringtie2unfiltered)
    path(gmapIndex)

    output:
    path "transdecoder.stringtie2.gff3", emit: unfiltered_genemodel
 
    script:
    """
    gmap -a 1 -D ${gmapIndex} -d gmap -f gff3_gene ${stringtie2unfiltered} --fulllength --nthreads=${task.cpus} --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > transdecoder.stringtie2.gff3 2> gmap.error
    """
}
process add_start_site {
    label 'process_medium'
    publishDir "$params.outdir/06_predictions/stringtie2",  mode: 'copy'

     	conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }      

    input:
    path(transdecoder)
    path(genome)

    output:
    path "gmap.stringtie2.gff3", emit: trans_gmap
 
    script:
    """
    fold ${genome} > genome.fasta
    agat_sp_add_start_and_stop.pl --gff ${transdecoder} --fasta genome.fasta --out start_stop.gff
    awk -F'\t' -v OFS='\t' '{\$3=\$3}1' start_stop.gff | sed 's/mRNA/transcript/g' > gmap.stringtie2.gff3
    """
}


process cluster_stringtie2 {
    label 'process_medium'    
    publishDir "$params.outdir/04_assembly/stringtie2/clustering",  mode: 'copy'
    module "${projectDir}/bin/usearch"

    // no available conda or singluarity 

    input:
    path(stringtie2transdecoderCDS)
    val(clust_id)

    output:
    path "*.cds", emit: centroids
    path "centroids.uc", emit: uc
 
    script:
    """
    usearch --cluster_fast ${stringtie2transdecoderCDS} --centroids centroids.cds --uc centroids.uc --id ${clust_id}
    """

}
  
process getComplete_stringtie2 {
    publishDir "$params.outdir/04_assembly/stringtie2/clustering",  mode: 'copy'
    label 'process_low'  


    	conda (params.enable_conda ? "bioconda::samtools=1.9" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/samtools:1.9--h91753b0_8"
    } else {
        container "quay.io/biocontainers/samtools:1.9--h91753b0_8"
    }   

    input:
    path(stringtie2Centroids)

    output:
    path "*.cds", emit: completeORF
    path "*.txt", emit: completeOutput
 
    script:
    """
    grep  "type:complete" ${stringtie2Centroids} | awk '{print \$1}' > id.txt 
sed -i -e 's/>//g' id.txt
while read line;
	do samtools faidx ${stringtie2Centroids} \$line >> stringtie.completeORF.cds;
done < id.txt
    """
}

process align_stringtie2 {
    publishDir "$params.outdir/04_assembly/stringtie2",  mode: 'copy'
    label 'process_medium'

    	conda (params.enable_conda ? "bioconda::gmap" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/gmap:2021.08.25--pl5321h67092d7_2"
    } else {
        container "quay.io/biocontainers/gmap:2021.08.25--pl5321h67092d7_2"
    }  
    
    input:
    path(stringtie2CompleteORF)
    path(gmapIndex)

    output:
    path "model.stringtie.gff3", emit: GeneModel
 
    script:
    """
    gmap -a 1 -D ${gmapIndex} -d gmap -f gff3_gene ${stringtie2CompleteORF} --fulllength --nthreads=${task.cpus} --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > model.stringtie.gff3 2> gmap.error

    """
}