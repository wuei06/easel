process proteinHints_orthodb {
    publishDir "$params.outdir/05_hints/orthoDB",  mode: 'copy' 
    label 'process_high'

    //conda (params.enable_conda ? "bioconda::miniprot" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/miniprot:0.7--h7132678_0"
    } else {
        container "quay.io/biocontainers/miniprot:0.7--h7132678_0"
    }

    input:
    path(script)
    path(genome)
    val(orthodb)
    path(bin)
    path(miniprot)
    val(N)
    val(outn)
    val(max_intron)

    output:
    path "orthodb.protHints.gff", emit: protein

    script:
    """
    if echo "${orthodb}" | grep -q -e 'embryophyta' -e 'virdiplantae'
    then
    wget --no-check-certificate https://v100.orthodb.org/download/odb10_plants_fasta.tar.gz
    tar xvf odb10_plants_fasta.tar.gz
    cat plants/Rawdata/* > proteins.fasta
    else
    wget --no-check-certificate https://v100.orthodb.org/download/odb10_${orthodb}_fasta.tar.gz
    tar xvf odb10_${orthodb}_fasta.tar.gz
    cat ${orthodb}/Rawdata/* > proteins.fasta
    fi

    ${miniprot}/miniprot -N ${N} --outn=${outn} -G ${max_intron} -t ${task.cpus} ${genome} proteins.fasta --gff-only > miniprot.gff
    ${script}/align2hints.pl --in=miniprot.gff --out=orthodb.protHints.gff --prg=gth
    """
}
process augustusProtein_psiclass {
    label 'process_medium'
    tag { id }

    	conda (params.enable_conda ? "bioconda::augustus" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/augustus:3.5.0--pl5321hf46c7bb_1"
    } else {
        container "quay.io/biocontainers/augustus:3.5.0--pl5321hf46c7bb_1"
    }

    input:
    path(gb)
    path(proteinHints)
    path(script)
    path(configProtein)
    path(AUGUSTUS_CONFIG_PATH)
    path(bin)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*psiclass.orthodb_protHints.gff3", emit: psiclass_proteinHints
      
    """
    ${bin}/augustus --species="$species"_psiclass ${genome_chunk} --extrinsicCfgFile=${configProtein} --alternatives-from-evidence=true --hintsfile=${proteinHints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_psiclass.orthodb_protHints.gff3

    """
}
process augustusProtein_stringtie2 {
    label 'augustus'
    tag { id }

    	conda (params.enable_conda ? "bioconda::augustus" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/augustus:3.5.0--pl5321hf46c7bb_1"
    } else {
        container "quay.io/biocontainers/augustus:3.5.0--pl5321hf46c7bb_1"
    }

    input:
    path(gb)
    path(proteinHints)
    path(script)
    path(configProtein)
    path(AUGUSTUS_CONFIG_PATH)
    path(bin)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*stringtie2.orthodb_protHints.gff3", emit: stringtie2_proteinHints
    
    script:  
    """
    ${bin}/augustus --species="$species"_stringtie2 ${genome_chunk} --extrinsicCfgFile=${configProtein} --alternatives-from-evidence=true --hintsfile=${proteinHints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_stringtie2.orthodb_protHints.gff3

    """
}
process combineProtein_stringtie2 {
    publishDir "$params.outdir/06_predictions/stringtie2",  mode: 'copy'
    label 'process_medium'
    tag { id }

    	conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }

    input:
    path(proteinHints)
     
    output:
    path("orthodb_protein_stringtie2.gff"), emit: stringtie_proteinHints
      
    """
mkdir protein
mv ${proteinHints} protein
for f in protein/*.gff3
do
    if wc -l \$f < 25; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
gff=\$(echo \$files)

agat_sp_merge_annotations.pl \$gff -o orthodb_protein_stringtie2.gff

    """
}
process combineProtein_psiclass {
    publishDir "$params.outdir/06_predictions/psiclass",  mode: 'copy'
    label 'process_medium'
    tag { id }

    	conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }

    input:
    path(proteinHints)
     
    output:
    path("orthodb_protein_psiclass.gff"), emit: psiclass_proteinHints
      
    """
mkdir protein
mv ${proteinHints} protein
for f in protein/*.gff3
do
    if wc -l \$f < 25; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
gff=\$(echo \$files)

agat_sp_merge_annotations.pl \$gff -o orthodb_protein_psiclass.gff

    """
}
