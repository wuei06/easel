process fetch_rna {
	publishDir "$params.outdir/01_reads/raw_reads",  mode: 'copy'
	label "process_medium"	
	tag { sra } 

    	conda (params.enable_conda ? "bioconda::sra-tools=2.11.0" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/sra-tools:2.11.0--pl5321ha49a11a_3"
    } else {
        container "quay.io/biocontainers/sra-tools:2.11.0--pl5321ha49a11a_3"
    }

	input:
	val(sra)

	output:
	tuple val(sra), path("*.fastq.gz"), optional: true, emit: sra

	script:
	"""
	fasterq-dump --split-files ${sra}
	gzip *.fastq 
	"""
}

process fastp {
	
	publishDir "$params.outdir/01_reads/quality_control",  mode: 'copy', pattern: '*.json'
	publishDir "$params.outdir/01_reads/quality_control",  mode: 'copy', pattern: '*.html'
    label "process_medium"
    tag { id }

    	conda (params.enable_conda ? "bioconda::fastp=0.23.2" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/fastp:0.23.2--hb7a2d85_2"
    } else {
        container "quay.io/biocontainers/fastp:0.23.2--hb7a2d85_2"
    }

   	input:
	tuple val(id), path(reads)

    output:
    tuple val(id), path('*.fastq.gz'), emit: trimmed
	tuple val(id), path('*.json'), emit: json
    tuple val(id), path('*.html'), emit: html
	tuple val(id), path("*.json"), path("*.fastq.gz"), emit: trimmed_json
    
	
    script:

   	"""
    fastp -i ${reads[0]} -I ${reads[1]} -o ${id}_fastp_1.fastq.gz -O ${id}_fastp_2.fastq.gz --thread ${task.cpus} --json ${id}.json --html ${id}.html
   
    """
}

process remove_low_qc {
	publishDir "$params.outdir/01_reads/trimmed_reads",  mode: 'copy', pattern: '*.fastq.gz'
	tag { id }	
	label 'process_single'

    input:
	tuple val(id), path(json), path(fastq)
    val(reads)
	val(mean_length)

    output:
	tuple val(id), path("*.fastq.gz"), optional: true, emit: pass_fastq
	path("*qc.txt"), emit: qc

    script:
    """ 
	reads=\$(sed -n '17p' ${json} | awk '{print \$1;}' | cut -f1 -d"," | sed 's/.*://')
	mean_length=\$(sed -n '23p' ${json} | awk '{print \$1;}' | cut -f1 -d"," | sed 's/.*://')
    reads_var=\$(echo \$reads)
	mean_length_var=\$(echo \$mean_length)
	
    if [ "\${reads_var}" -lt "${reads}" && "\${mean_length_var}" -lt "${mean_length}" ]
	then
	mv ${fastq[0]} ${id}_1_remove.fq.gz
	mv ${fastq[1]} ${id}_2_remove.fq.gz
	echo "${id} has less than ${reads} reads and has been removed" > ${id}_qc.txt
	else
	mv ${fastq[0]} ${id}_trimmed_1.fastq.gz
	mv ${fastq[1]} ${id}_trimmed_2.fastq.gz
	echo "${id} has more than ${reads} reads and was kept" 
	gc=\$(sed -n '25p' ${json} | awk '{print \$1;}' | cut -f1 -d"," | sed 's/.*://')
	echo "${id} \$reads_var \$mean_length_var \$gc" > ${id}_qc.txt
	fi 

    """
}
process log_qc {
	label 'process_single'
	publishDir "$params.outdir/log",  mode: 'copy'

    input:
	path(qc)
	val(reads)
	val(mean_length)

	output:
	path("log_qc.txt"), emit: log_qc

    script:
    """ 
	header="library total_bases read_mean_length gc_content"
	echo " " > log_qc.txt
	echo "##### RNA Reads #####" >> log_qc.txt
	
	mkdir reads
	mkdir removed
	mkdir kept
	mv ${qc} reads

	for f in reads/*
	do
	if grep -q 'reads and has been removed' \$f
	then
    mv \$f removed
	else
	mv \$f kept
	fi
	done

	if [ -z "\$(ls -A kept)" ]; then
    exit 1
	echo "All files were removed"
	else
    cat kept/* > kept.txt
	echo "Passed:" >> log_qc.txt
	echo "\$header" >> log_qc.txt
	cat kept.txt | while read line; do echo "\$line"; done >> log_qc.txt
	fi

	if [ -z "\$(ls -A removed)" ]; then
	echo "" >> log_qc.txt
    echo "Failed:" >> log_qc.txt
    echo "None." >> log_qc.txt
	echo "All libraries have more than ${reads} total bases and a mean read length greater than ${mean_length}" >> log_qc.txt
	else
    cat removed/* > removed.txt
	echo "" >> log_qc.txt
	echo "Failed:" >> log_qc.txt
	cat removed.txt | while read line; do echo "\$line"; done >> log_qc.txt
	fi
	
	
    """
}