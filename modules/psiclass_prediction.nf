process ESThints_psiclass {
    publishDir "$params.outdir/05_hints/psiclass",  mode: 'copy'
    label 'process_high'

    	conda (params.enable_conda ? "bioconda::gmap" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/gmap:2021.08.25--pl5321h67092d7_2"
    } else {
        container "quay.io/biocontainers/gmap:2021.08.25--pl5321h67092d7_2"
    }  

    input:
    path(gmapIndex)
    path(psiclasstransdecoderCDS)
    path(script)
     
    output:
    path "eggnog.estHints.gff", emit: psiclass_est
      
    """
    gmap -D ${gmapIndex} -d gmap ${psiclasstransdecoderCDS} --nthreads=${task.cpus} --min-intronlength=30 --intronlength=500000 --trim-end-exons=20 -f 1 -n 0 > gmap_Eggnog.psl 
    cat gmap_Eggnog.psl | sort -n -k 16,16 | sort -s -k 14,14 | perl -ne '@f=split; print if (\$f[0]>=100)' | ${script}/blat2hints.pl --source=PB --nomult --ep_cutoff=20 --in=/dev/stdin --out=eggnog.estHints.gff
	
    """
}
process proteinHints_psiclass {
    publishDir "$params.outdir/05_hints/psiclass",  mode: 'copy' 
    label 'process_high'

    	conda (params.enable_conda ? "bioconda::genomethreader" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/genomethreader:1.7.1--h87f3376_4"
    } else {
        container "quay.io/biocontainers/genomethreader:1.7.1--h87f3376_4"
    }  

    input:
    path(script)
    path(genome)
    path(psiclasstransdecoderPEP)
    path(bin)
    path(miniprot)
    val(N)
    val(outn)
    val(max_intron)

    output:
    path "eggnog.protHints.gff", emit: psiclass_protein

    script:
    if (params.aligner == 'gth')
    """
    ${script}/startAlign.pl --genome=${genome} --prot=${psiclasstransdecoderPEP} --prg=gth --CPU=${task.cpus}
    ${script}/align2hints.pl --in=align_gth/gth.concat.aln --out=eggnog.protHints.gff --prg=gth
    """
    else if (params.aligner == 'miniprot')
    """
    ${miniprot}/miniprot -N ${N} --outn=${outn} -G ${max_intron} -t ${task.cpus} ${genome} ${psiclasstransdecoderPEP} --gff-only > miniprot.gff
    ${script}/align2hints.pl --in=miniprot.gff --out=eggnog.protHints.gff --prg=gth
    """
}
process trainingSet_psiclass {
    label 'process_low'
    tag { id }

    	conda (params.enable_conda ? "bioconda::augustus" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/augustus:3.5.0--pl5321hf46c7bb_1"
    } else {
        container "quay.io/biocontainers/augustus:3.5.0--pl5321hf46c7bb_1"
    }  

    input:
    path(psiclassGmap)
    path(genome)
    path(AUGUSTUS_CONFIG_PATH)
    path(script)
    path(bin)
    val(species)
    val(test)
    val(train)
     
    output:
    path "*.gb", emit: gb
      
    """
    gffread ${psiclassGmap} -T -o gmap.psiclass.gtf
    ${script}/computeFlankingRegion.pl gmap.psiclass.gtf | tee calculate_flank.out
    flanking_DNA=\$(grep -A0 "flanking_DNA" calculate_flank.out | awk '{print \$5}')
    echo "\$flanking_DNA" >> calculate_flank.out
    ${script}/gff2gbSmallDNA.pl ${psiclassGmap} ${genome} \$flanking_DNA psiclassRaw.gb
    ${script}/new_species.pl --species="$species"_psiclass
    ${bin}/etraining --species="$species"_psiclass psiclassRaw.gb 2> failed_genes.txt
    awk '{print \$7}' failed_genes.txt | sed s/://g > badTrainingGenes.lst
    ${script}/filterGenes.pl badTrainingGenes.lst psiclassRaw.gb > psiclassGoodGenes.gb
    grep -c "LOCUS" *.gb | tee calculate_flank.out
    ${script}/randomSplit.pl psiclassGoodGenes.gb ${test}
    ${script}/randomSplit.pl psiclassGoodGenes.gb.train ${train}
    rm psiclassGoodGenes.gb.train
    rm psiclassGoodGenes.gb.train.train
    mv psiclassGoodGenes.gb.train.test train.gb
    mv psiclassGoodGenes.gb.test test.gb
    grep -c "LOCUS" *.gb >> calculate_flank.out
    ${bin}/etraining --species="$species"_psiclass train.gb
    ${bin}/augustus --species="$species"_psiclass | tee psiclass_untrained.out
    ${script}/optimize_augustus.pl --species="$species"_psiclass train.gb --cpus=${task.cpus} --kfold=12 --aug_exec_dir ${AUGUSTUS_CONFIG_PATH}
    ${bin}/etraining --species="$species"_psiclass train.gb
    ${bin}/augustus --species="$species"_psiclass test.gb | tee psiclass_optimized.out

    """
}

process augustusEST_psiclass {
    label 'process_medium'
    tag { id }

    	conda (params.enable_conda ? "bioconda::augustus" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/augustus:3.5.0--pl5321hf46c7bb_1"
    } else {
        container "quay.io/biocontainers/augustus:3.5.0--pl5321hf46c7bb_1"
    }  

    input:
    path(gb)
    path(ESThints)
    path(script)
    path(configEST)
    path(AUGUSTUS_CONFIG_PATH)
    path(bin)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*psiclass.ESTHints.gff3", emit: psiclass_estHints
      
    """
    ${bin}/augustus --species="$species"_psiclass ${genome_chunk} --extrinsicCfgFile=${configEST} --alternatives-from-evidence=true --hintsfile=${ESThints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_psiclass.ESTHints.gff3
    """
}

process augustusProtein_psiclass {
    label 'process_medium'
    tag { id }

    	conda (params.enable_conda ? "bioconda::augustus" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/augustus:3.5.0--pl5321hf46c7bb_1"
    } else {
        container "quay.io/biocontainers/augustus:3.5.0--pl5321hf46c7bb_1"
    }  

    input:
    path(gb)
    path(proteinHints)
    path(script)
    path(configProtein)
    path(AUGUSTUS_CONFIG_PATH)
    path(bin)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*psiclass.protHints.gff3", emit: psiclass_proteinHints
      
    """
    ${bin}/augustus --species="$species"_psiclass ${genome_chunk} --extrinsicCfgFile=${configProtein} --alternatives-from-evidence=true --hintsfile=${proteinHints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_psiclass.protHints.gff3

    """
}

process combineEST_psiclass {
    publishDir "$params.outdir/06_predictions/psiclass",  mode: 'copy'
    label 'process_medium'
    tag { id }

    	conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }

    input:
    path(estHints)
     
    output:
    path("est_psiclass.gff"), emit: estGFF
      
    """
mkdir est
mv ${estHints} est
for f in est/*.gff3
do
    if wc -l \$f < 25; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
gff=\$(echo \$files)

agat_sp_merge_annotations.pl \$gff -o est_psiclass.gff

    """
}

process combineProtein_psiclass {
    publishDir "$params.outdir/06_predictions/psiclass",  mode: 'copy'
    label 'process_medium'
    tag { id }

    	conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }

    input:
    path(proteinHints)
     
    output:
    path("protein_psiclass.gff"), emit: psiclass_proteinHints
      
    """
mkdir protein
mv ${proteinHints} protein
for f in protein/*.gff3
do
    if wc -l \$f < 25; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
gff=\$(echo \$files)

agat_sp_merge_annotations.pl \$gff -o protein_psiclass.gff

    """
}

process combineAll_psiclass {
    publishDir "$params.outdir/06_predictions/psiclass",  mode: 'copy'
    label 'process_medium'
    tag { id }

    	conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }
    
    input:
    path(augustus_protein)
    path(augustus_est)
     
    output:
    path("psiclass_unfiltered.gff"), emit: psiclass_combined
      
    """

agat_sp_merge_annotations.pl -f ${augustus_protein} -f ${augustus_est} -o psiclass_unfiltered.gff

    """
}