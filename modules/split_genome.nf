process percent_masked {
    label 'process_single'
    publishDir "$params.outdir/log",  mode: 'copy'

    input:
    path(genome)

    output:
    path("log_genome.txt"), emit: log_genome
      
    """
masked_bases=\$(grep -o "[a-z]" ${genome} | grep -v "^>" | wc -l)
genome_length=\$(grep -o "[A-Za-z]" ${genome} | grep -v "^>" | wc -l)
masked_percentage=\$(echo "(\$masked_bases/\$genome_length)*100" | bc -l | xargs printf "%.2f")
high=\$(echo "25")
low=\$(echo "10")

if [[ "\$masked_percentage" > "\$low" && "\$masked_percentage" < "\$high" ]]
then
echo "##### Genome #####" >> log_genome.txt
echo "\$masked_percentage % of ${genome} is masked" >> log_genome.txt
echo "WARNING: Less than 25% masked" >> log_genome.txt
elif [[ "\$masked_percentage" < "\$low" ]]
then
echo "######## Genome ########" >> log_genome.txt
echo "\$masked_percentage % of ${genome} is masked" >> log_genome.txt
echo "FATAL ERROR: Less than 10% of ${genome} is masked" >> log_genome.txt
exit 1
else
echo "######## Genome ########" >> log_genome.txt
echo "\$masked_percentage % of ${genome} is masked" >> log_genome.txt
fi
    """
}
process split_genome {
    label 'process_single'
    
    input:
    path(genome)
    path(scripts)
    val(bins)

    output:
    path("*.f*"), emit: chromosomes
      
    """
    if grep -q "^[acgt]" ${genome}
    then
    ${scripts}/fasta-splitter.pl --n-parts ${bins} ${genome} 
    else
    echo "ERROR: ${genome} is not masked"
    exit 1
    fi
    """
} 

