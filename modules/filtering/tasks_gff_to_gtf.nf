process gtf {
    label 'process_low'

    conda (params.enable_conda ? "bioconda::gffread" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/gffread:0.9.12--0"
    } else {
        container "quay.io/biocontainers/gffread:0.9.12--0"
    }

    input:
    path(gff)
     
    output: 
    path("unfiltered.gtf"), emit: unfiltered_gtf
      
    """
   gffread -E ${gff} -T -o unfiltered.gtf

    """
}


