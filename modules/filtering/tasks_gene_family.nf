process filterEggNOG {
    label 'process_medium'
     
        conda (params.enable_conda ? "bioconda::agat bioconda::eggnog-mapper" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0:https://depot.galaxyproject.org/singularity/eggnog-mapper:2.1.9--pyhdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0:quay.io/biocontainers/eggnog-mapper:2.1.9--pyhdfd78af_0"
    }

    input:
    path(genome)
    path(unfiltered_prediction)
    path(scripts)
    path(matrix)
     
    output: 
    path("eggnogHits.txt"), emit: txt
    path("protein.pep"), emit: protein
      
    """
    fold ${genome} > genome.fasta 
    agat_sp_extract_sequences.pl -g ${unfiltered_prediction} -f genome.fasta --protein -o protein.pep 

    emapper.py -i protein.pep -o eggnog --override --no_annot -m diamond --cpu 16
    awk '{print \$1}' eggnog.emapper.hits | sort | uniq > ids.txt 

    awk -F '\t' -v OFS='\t' '{ \$(NF+1) = 1; print }' ids.txt > eggnogHits.txt
    sed -i \$'1 i\\\nTranscript\tHit' eggnogHits.txt

    """
}

process pythonEggNOG {
    label 'process_low'
     
    input:
    path(matrix)
    path(scripts)
    path(txt)
     
    output: 
    path("eggnog.tracking"), emit: eggnog
      
    """
    python ${scripts}/add_eggnog.py ${matrix} ${txt}
    awk 'BEGIN { FS=OFS="\t" } NR > 1 { \$4 = sprintf("%d", \$4) }; 1' OFS="\t" feature.tracking > eggnog.tracking

    """
}