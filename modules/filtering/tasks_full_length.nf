process filterLength {
    label 'process_low'

    	conda (params.enable_conda ? "bioconda::gffread bioconda::gffcompare" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/gffcompare:0.11.2--h9f5acd7_3:https://depot.galaxyproject.org/singularity/gffread:0.9.12--0"
    } else {
        container "quay.io/biocontainers/gffcompare:0.11.2--h9f5acd7_3:quay.io/biocontainers/gffread:0.9.12--0"
    }
    
    input:
    path(genome)
    path(unfiltered_prediction)
    path(list)
    path(scripts)
    path(matrix)
     
    output: 
    path("full.txt"), emit: txt
      
    """
    gffread ${unfiltered_prediction} -J -g ${genome} -o startStop.gff
    gffcompare -r startStop.gff -i ${list} -T -o full_1
    awk '{split (\$3, T, "|"); \$3 = T[1] OFS T[2]}1' OFS="\t" full_1.tracking > full_2.tracking
    awk '{ print \$4 }' OFS="\t" full_2.tracking > full_3.tracking
    awk -F '\t' -v OFS='\t' '{ \$(NF+1) = 1; print }' full_3.tracking > full.txt
    sed -i \$'1 i\\\nTranscript\tHit' full.txt

    """
}

process pythonLength {
    label 'process_low'
     
    input:
    path(matrix)
    path(scripts)
    path(txt)
     
    output: 
    path("full_length.tracking"), emit: full_length
      
    """
    python ${scripts}/add_startStop.py ${matrix} ${txt}
    awk 'BEGIN { FS=OFS="\t" } NR > 1 { \$4 = sprintf("%d", \$4) }; 1' OFS="\t" feature.tracking > full_length.tracking

    """
}