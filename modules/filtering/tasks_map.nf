process filterMap {
    publishDir "$params.outdir/features",  mode: 'copy'
    label 'process_single'
     
    input:
    path(gc_ratio)
    path(rna_fold)
    path(gc_content)
    path(length)
    path(weight)
    path(support)
    path(multi_mono)
    path(gene_family)
    path(full_length)
    path(splice_site)
    path(start_site)
    path(cds_repeat)
    path(cds_length)
     
    output: 
    path("features.tracking"), emit: features
      
    """
    awk '{print \$4}' ${gc_ratio} > gc_ratio.txt
    awk '{print \$4}' ${gc_content} > gc_content.txt
    awk '{print \$4}' ${rna_fold} > rna_fold.txt
    awk '{print \$4}' ${length} > length.txt
    awk '{print \$4}' ${weight} > weight.txt
    awk '{print \$4}' ${multi_mono} > multi_mono.txt
    awk '{print \$4}' ${full_length} > full_length.txt
    awk '{print \$4}' ${gene_family} > gene_family.txt
    awk '{print \$4}' ${splice_site} > splice_site.txt
    awk '{print \$4}' ${start_site} > start_site.txt
    awk '{print \$4}' ${cds_repeat} > cds_repeat.txt
    awk '{print \$4}' ${cds_length} > cds_length.txt
    awk '{print \$2,\$3 }' ${support} > support.txt

    paste ${support} multi_mono.txt gene_family.txt full_length.txt splice_site.txt start_site.txt weight.txt length.txt cds_repeat.txt gc_content.txt rna_fold.txt gc_ratio.txt | column -s \$'\t' -t > features.tracking

    """
}