process true_start_site {
    label 'process_single'
    
    input:
    path(unfiltered_prediction)

    output:
    path("true_start_sites.txt"), emit: text

    """
    awk '\$3 == "start_codon" {
    match(\$9, /Parent=([^;]+)/, arr); OFS="\t";
    print \$1, \$4, \$7, arr[1]
    }' ${unfiltered_prediction} > true_start_sites.txt
    """
}
process pad {     
    label 'process_low'
    
    input:
    val(windows)
    path(true_start_sites)
    path(genome)
    path(scripts)
     
    output: 
    path("*true_start_sites.fa"), emit: fasta
      
    """
var1=\$(echo ${windows} | cut -f1 -d"_")
var2=\$(echo ${windows} | cut -f2 -d"_" | cut -f1 -d"/")
python ${scripts}/pad.py --start_sites ${true_start_sites} --fasta ${genome} --out ${windows}_true_start_sites.fa --up \$var1 --down \$var2   

    """
}
process split_fasta {
    label 'process_low'

    input:
    path(start_fasta)
    path(scripts)
    val(parts)
    
    output:
    path("*.fa"), emit: split

    """
    ${scripts}/fasta-splitter.pl --n-parts ${parts} ${start_fasta}

    """

}
process rna_fold {
    label 'process_medium'
    tag {id}
 
    conda (params.enable_conda ? "bioconda::seqtk bioconda::viennarna" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/seqtk:1.3--h7132678_4:https://depot.galaxyproject.org/singularity/viennarna:2.5.1--py39pl5321h2add14b_0"
    } else {
        container "quay.io/biocontainers/seqtk:1.3--h7132678_4:quay.io/biocontainers/viennarna:2.5.1--py39pl5321h2add14b_0"
    }

    input:
    tuple val(id), path(start_fasta)
     
    output: 
    path("*rna.out"), emit: rna_fold
      
    """
seqtk seq ${start_fasta} | cat | while read line; do                                                                                                                                                                 
if [[ "\$line" =~ ^\\>.* ]]
    then
    echo \$line >> ${id}.rna.out
    else
    echo -e \$line | RNAfold --verbose --noPS >> ${id}.rna.out
    fi
done      
    """
}
process merge {
    label 'process_single'
    
    publishDir "$params.outdir/07_filtering",  mode: 'copy'
    tag {id}

    input:
    path(rna)
    
    output: 
    path("merged_rna.out"), emit: merge_rna
      
    """
    cat ${rna} > merged_rna.out                                                                                         
    """
}
process matrix {
    label 'process_medium'

    input:
    path(scripts)
    val(windows)
    path(matrix)
    path(folding)
     
    output: 
    path("free_energy.tracking"), emit: free_energy
    path("gc_ratio.tracking"), emit: gc_ratio
      
    """
    var1=\$(echo ${windows} | cut -f1 -d"_")
    var2=\$(echo ${windows} | cut -f2 -d"_" | cut -f1 -d"/")
    python ${scripts}/folding_features.py ${folding} \$var1 \$var2 folding_features.txt

    sed -i 's/"//g' folding_features.txt
    awk 'BEGIN { OFS="\t" } { print \$1, \$2 }' folding_features.txt > free_energy.txt
    sed -i \$'1 i\\\nTranscript\tHit\' free_energy.txt
    awk 'BEGIN { OFS="\t" } { print \$1, \$3 }' folding_features.txt > GC_ratio.txt
    sed -i \$'1 i\\\nTranscript\tHit\' GC_ratio.txt

    python ${scripts}/add_free_energy.py ${matrix} free_energy.txt
    python ${scripts}/add_gc_ratio.py ${matrix} GC_ratio.txt

    """
}