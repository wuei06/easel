process filterSupport {
    label 'process_low'

    	conda (params.enable_conda ? "bioconda::gffcompare" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/gffcompare:0.11.2--h9f5acd7_3"
    } else {
        container "quay.io/biocontainers/gffcompare:0.11.2--h9f5acd7_3"
    }
     
    input:
    path(unfiltered_prediction)
    path(list)
     
    output: 
    path("support.tracking"), emit: support
      
    """
    gffcompare -r ${unfiltered_prediction} -i ${list} -T -o support_1
    awk '{split (\$3, T, "|"); \$3 = T[1] OFS T[2]}1' OFS="\t" support_1.tracking > support_2.tracking
    awk '{ print \$3, \$4, \$6, \$7, \$8, \$9}' OFS="\t" support_2.tracking > support_3.tracking 

    awk -F"\t" -v OFS="\t" '{ for(N=3; N<=NF; N++) { if(\$N ~ /^q/) \$N="1" ; if(\$N ~ /^-/) \$N="0" } } 1' support_3.tracking > support_4.tracking
    awk '{sum3+=\$3; sum4+=\$4; sum5+=\$5; sum6+=\$6}''{print \$1,\$2,(\$3+\$4+\$5+\$6)-1}' OFS="\t" support_4.tracking > support_5.tracking
    sort support_5.tracking | uniq > unique.txt
    sort -t\$'\t' -k3nr unique.txt | awk -F'\t' '!a[\$2]++' > support.tracking
    sed -i \$'1 i\\\nGene\tTranscript\tSupport' support.tracking

    """
}

process filterSupport_orthodb {
    label 'process_low'

        conda (params.enable_conda ? "bioconda::gffcompare" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/gffcompare:0.11.2--h9f5acd7_3"
    } else {
        container "quay.io/biocontainers/gffcompare:0.11.2--h9f5acd7_3"
    }
     
    input:
    path(unfiltered_prediction)
    path(list)
     
    output: 
    path("support.tracking"), emit: support_db
      
    """
    gffcompare -r ${unfiltered_prediction} -i ${list} -T -o support_1
    awk '{split (\$3, T, "|"); \$3 = T[1] OFS T[2]}1' OFS="\t" support_1.tracking > support_2.tracking
    awk '{ print \$3, \$4, \$6, \$7, \$8, \$9, \$10, \$11 }' OFS="\t" support_2.tracking > support_3.tracking 

    awk -F"\t" -v OFS="\t" '{ for(N=3; N<=NF; N++) { if(\$N ~ /^q/) \$N="1" ; if(\$N ~ /^-/) \$N="0" } } 1' support_3.tracking > support_4.tracking
    awk '{sum3+=\$3; sum4+=\$4; sum5+=\$5; sum6+=\$6; sum7+=\$7; sum8+=\$8}''{print \$1,\$2,(\$3+\$4+\$5+\$6+\$7+\$8)-1}' OFS="\t" support_4.tracking > support_5.tracking
    sort support_5.tracking | uniq > unique.txt
    sort -t\$'\t' -k3nr unique.txt | awk -F'\t' '!a[\$2]++' > support.tracking
    sed -i \$'1 i\\\nGene\tTranscript\tSupport' support.tracking

    """
}
