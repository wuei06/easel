process filterORF {
    label 'process_medium'

    	conda (params.enable_conda ? "bioconda::agat bioconda::gffcompare" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0:https://depot.galaxyproject.org/singularity/gffcompare:0.11.2--h9f5acd7_3"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0:quay.io/biocontainers/gffcompare:0.11.2--h9f5acd7_3"
    }

    input:
    path(unfiltered_prediction)
    path(list)
    path(scripts)
    path(matrix)
     
    output: 
    path("orf.txt"), emit: txt
      
    """
    agat_sp_filter_by_ORF_size.pl --gff ${unfiltered_prediction} -o filtered_orf.gff
    gffcompare -r filtered_orf_sup100.gff -i ${list} -T -o orf_1
    awk '{split (\$3, T, "|"); \$3 = T[1] OFS T[2]}1' OFS="\t" orf_1.tracking > orf_2.tracking
    awk '{ print \$4 }' OFS="\t" orf_2.tracking > orf_3.tracking
    awk -F '\t' -v OFS='\t' '{ \$(NF+1) = 1; print }' orf_3.tracking > orf.txt
    sed -i \$'1 i\\\nTranscript\tHit' orf.txt

    """
}

process pythonORF {
    label 'process_low'
     
    input:
    path(matrix)
    path(scripts)
    path(txt)
     
    output: 
    path("orf_size.tracking"), emit: orf
      
    """
    python ${scripts}/add_orf_size.py ${matrix} ${txt}
    awk 'BEGIN { FS=OFS="\t" } NR > 1 { \$4 = sprintf("%d", \$4) }; 1' OFS="\t" feature.tracking > orf_size.tracking

    """
}