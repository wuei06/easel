process filterTranscript {
    label 'process_medium'

    	conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }

    input:
    path(genome)
    path(unfiltered_prediction)
    path(matrix)

     
    output: 
    path("transcript.fasta"), emit: transcript
      
    """
    fold ${genome} > genome.fasta 
    awk '{ print \$2 }' ${matrix} > transcript_list.txt
    agat_sp_extract_sequences.pl -g ${unfiltered_prediction} -f genome.fasta -t transcript -o transcript.fasta

    """
}

process pythonTranscriptLength {
    label 'process_low'
     
    input:
    path(transcript)
    path(scripts)
    path(matrix)
     
    output: 
    path("transcript_length.tracking"), emit: length
      
    """
    python ${scripts}/add_percentage_masked_cds.py ${transcript}
    
    awk '{ print \$1, \$3 }' OFS="\t" masked.tracking > transcript_length.txt
    sed -i \$'1 i\\\nTranscript\tHit' transcript_length.txt

    python ${scripts}/add_transcript_length.py ${matrix} transcript_length.txt

    """
}

process pythonMolecularWeight {
    label 'process_low'
     
    input:
    path(transcript)
    path(scripts)
    path(matrix)
     
    output: 
    path("transcript_weight.tracking"), emit: weight
      
    """
    python ${scripts}/add_molecular_weight.py ${transcript}
    
    awk '{ print \$1, \$4 }' OFS="\t" masked.tracking > transcript_length.txt
    sed -i \$'1 i\\\nTranscript\tHit' transcript_length.txt

    python ${scripts}/add_transcript_weight.py ${matrix} transcript_length.txt

    """
}

process filterGC {
    label 'process_low'

    	conda (params.enable_conda ? "bioconda::seqkit" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/seqkit:2.3.1--h9ee0642_0"
    } else {
        container "quay.io/biocontainers/seqkit:2.3.1--h9ee0642_0"
    }
     
    input:
    path(transcript)
    path(scripts)
    path(matrix)
     
    output: 
    path("gc_content.tracking"), emit: gc
      
    """
    seqkit fx2tab --name --gc ${transcript} > gc.txt 
    awk '{ print \$1, \$5 }' OFS="\t" gc.txt > gc_ratio.txt

    sed -i \$'1 i\\\nTranscript\tHit' gc_ratio.txt

    python ${scripts}/add_gc_content.py ${matrix} gc_ratio.txt

    """
}