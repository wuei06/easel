process filterRepeatCDS {
    label 'process_medium'

    	conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }
     
    input:
    path(genome)
    path(unfiltered_prediction)
    path(matrix)
     
    output: 
    path("CDS.fasta"), emit: cds
      
    """
    fold ${genome} > genome.fasta 
    awk '{ print \$2 }' ${matrix} > transcripts_list.txt
    agat_sp_extract_sequences.pl -g ${unfiltered_prediction} -f genome.fasta -t cds -o CDS.fasta

    """
}

process pythonRepeatCDS {
    label 'process_low'
     
    input:
    path(cds)
    path(scripts)
    path(matrix)
     
    output: 
    path("cds_masked.tracking"), emit: cds_repeat
    path("cds_length.tracking"), emit: cds_length
      
    """

    python ${scripts}/add_percentage_masked_cds.py ${cds}
    
    awk '{ print \$1, \$6}' OFS="\t" masked.tracking > CDS_masked.txt
    sed -i \$'1 i\\\nTranscript\tHit' CDS_masked.txt
    awk '{ print \$1, \$3}' OFS="\t" masked.tracking > CDS_length.txt
    sed -i \$'1 i\\\nTranscript\tHit' CDS_length.txt

    python ${scripts}/add_repeat_cds.py ${matrix} CDS_masked.txt
    python ${scripts}/add_length_cds.py ${matrix} CDS_length.txt

    """
}
