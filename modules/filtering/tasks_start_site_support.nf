process filterStartSite {
    label 'process_medium'

    	conda (params.enable_conda ? "bioconda::agat bioconda::gffcompare" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0:https://depot.galaxyproject.org/singularity/gffcompare:0.11.2--h9f5acd7_3"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0:quay.io/biocontainers/gffcompare:0.11.2--h9f5acd7_3"
    }
     
    input:
    path(unfiltered_prediction)
    path(list)
    path(scripts)
    path(matrix)
    path(genome)
     
    output: 
    path("start_site.txt"), emit: txt
      
    """
    fold ${genome} > genome.fasta
    agat_sp_add_start_and_stop.pl --gff ${unfiltered_prediction} --fasta genome.fasta --out add_start_stop.gff
    agat_sp_extract_attributes.pl --gff add_start_stop.gff --att Parent -p start_codon -o start_codon.txt
    agat_sp_filter_feature_from_keep_list.pl --gff ${unfiltered_prediction} --keep_list start_codon_Parent.txt --attribute transcript_id -o start_codon.gff
    gffcompare -r start_codon.gff -i ${list} -T -o start_1

    awk '{split (\$3, T, "|"); \$3 = T[1] OFS T[2]}1' OFS="\t" start_1.tracking > start_2.tracking
    awk '{ print \$4 }' OFS="\t" start_2.tracking > start_3.tracking
    awk -F '\t' -v OFS='\t' '{ \$(NF+1) = 1; print }' start_codon_Parent.txt > start_site.txt
    sed -i \$'1 i\\\nTranscript\tHit' start_site.txt

    """
}

process pythonStartSite {
    label 'process_low'
     
    input:
    path(matrix)
    path(scripts)
    path(txt)
     
    output: 
    path("start_site.tracking"), emit: start_site
      
    """
    python ${scripts}/add_start_site.py ${matrix} ${txt}
    awk 'BEGIN { FS=OFS="\t" } NR > 1 { \$4 = sprintf("%d", \$4) }; 1' OFS="\t" feature.tracking > start_site.tracking

    """
}
