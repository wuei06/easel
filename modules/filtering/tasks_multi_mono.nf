process filterMulti {
    label 'process_low'

    	conda (params.enable_conda ? "bioconda::gffcompare" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/gffcompare:0.11.2--h9f5acd7_3"
    } else {
        container "quay.io/biocontainers/gffcompare:0.11.2--h9f5acd7_3"
    }
     
    input:
    path(unfiltered_prediction)
    path(list)
    path(scripts)
    path(matrix)
     
    output: 
    path("mono_multi.txt"), emit: txt
      
    """
    gffcompare -r ${unfiltered_prediction} -i ${list} -T -M -o multi_1
    awk '{split (\$3, T, "|"); \$3 = T[1] OFS T[2]}1' OFS="\t" multi_1.tracking > multi_2.tracking
    awk '{ print \$4 }' OFS="\t" multi_2.tracking > multi_3.tracking
    awk -F '\t' -v OFS='\t' '{ \$(NF+1) = 1; print }' multi_3.tracking > mono_multi.txt
    sed -i \$'1 i\\\nTranscript\tHit' mono_multi.txt

    """
}

process pythonMulti {
    label 'process_low'
     
    input:
    path(matrix)
    path(scripts)
    path(txt)
     
    output: 
    path("multi_mono.tracking"), emit: mono_multi
      
    """
    python ${scripts}/add_multi.py ${matrix} ${txt}
    awk 'BEGIN { FS=OFS="\t" } NR > 1 { \$4 = sprintf("%d", \$4) }; 1' OFS="\t" feature.tracking > multi_mono.tracking

    """
}