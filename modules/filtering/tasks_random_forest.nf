process regressor {
    label 'process_low'
    publishDir "$params.outdir/07_filtering",  mode: 'copy' 
    
    input:
    path(scripts)
    path(test)
    path(train)
     
    output: 
    path("regressor_prediction.csv"), emit: r_rf
      
    """
    python ${scripts}/random_forest_regressor_predict.py ${train} ${test} regressor_prediction.csv

    """
}
process classifier {
    label 'process_low'
    publishDir "$params.outdir/07_filtering",  mode: 'copy' 
    
    input:
    path(scripts)
    path(test)
    path(train)
     
    output: 
    path("classifier_prediction.csv"), emit: c_rf
      
    """
    python ${scripts}/random_forest_classifier_predict.py ${train} ${test} classifier_prediction.csv

    """
}
process score {
    label 'process_low'
    publishDir "$params.outdir/07_filtering",  mode: 'copy' 
    
    input:
    path(scripts)
    path(test)
     
    output: 
    path("score.tracking"), emit: score
      
    """
    python ${scripts}/score.py ${test}

    """
}
process filtered {
    label 'process_single'
    publishDir "$params.outdir/final_predictions",  mode: 'copy' 
    
    input:
    path(classifier)
    path(regressor)
    path(score)
    path(unfiltered_gtf)
     
    output: 
    path("filtered.gtf"), emit: filtered_prediction
      
    """
 
tail -n +2 ${classifier} | awk -F"," '\$2==1' > filtered.csv 

tail -n +2 ${regressor} | awk -F"," '\$2>90' >> filtered.csv 

tail -n +2 ${score} | awk -F"," '\$15==7' | cut -d',' -f2,15 >> filtered.csv 
awk -F"," '{ print \$1 }' filtered.csv | sort -u > transcripts.txt

grep -Fwf transcripts.txt ${unfiltered_gtf} > filtered.gtf

    """
}
process protein {
    label 'process_medium'

    	conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }
    
    input:
    path(filtered_prediction)
    path(genome)
     
    output: 
    path("protein.pep"), emit: protein
      
    """
    fold ${genome} > genome.fasta 
    agat_sp_extract_sequences.pl -g ${filtered_prediction} -f genome.fasta --protein -o protein.pep 

    """
}
