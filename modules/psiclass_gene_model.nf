process psiclass {
    publishDir "$params.outdir/04_assembly/psiclass/gtf",  mode: 'copy', pattern: "*.gtf"
    publishDir "$params.outdir/04_assembly/psiclass",  mode: 'copy', pattern: "*.fa"
    publishDir "$params.outdir/log",  mode: 'copy', pattern: "*.txt"

    tag { id }
    label 'process_medium'

    	conda (params.enable_conda ? "bioconda::psiclass" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/psiclass:1.0.3--h87f3376_1"
    } else {
        container "quay.io/biocontainers/psiclass:1.0.3--h87f3376_1"
    }
     
    input:
    path(bam)
    path(genome)
     
    output: 
    path("*.gtf"), emit: psiclass_gtf
    path("*.fa"), emit: fasta
    path("log_psiclass.txt"), emit: log_psiclass
      
    """
    bam=\$(echo '${bam}' | sed -e "s/ /,/g") 
    psiclass -b \$bam -p ${task.cpus}
    gffread -w psiclass_transcripts.fa -g ${genome} psiclass_vote.gtf

    transcripts=\$(echo | grep -c ">" psiclass_transcripts.fa)
    echo " " >> log_psiclass.txt
    echo "##### PsiCLASS Transcriptome Asssembly #####" >> log_psiclass.txt
    echo "\$transcripts transcripts" >> log_psiclass.txt
    """
}

process orf_psiclass {

    publishDir "$params.outdir/04_assembly/psiclass/frameselection",  mode: 'copy'
    label 'process_medium'
    tag { id }

    	conda (params.enable_conda ? "bioconda::transdecoder" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/transdecoder:5.5.0--pl5321hdfd78af_5"
    } else {
        container "quay.io/biocontainers/transdecoder:5.5.0--pl5321hdfd78af_5"
    }   


    input:
    path(psiclassTranscriptome) 
     
    output:
    path "${psiclassTranscriptome}.transdecoder_dir/longest_orfs.pep", emit:longOrfsPepFiles
    path "${psiclassTranscriptome}.transdecoder_dir/", emit:LongOrfsDirFiles
    path "${psiclassTranscriptome}.transdecoder_dir.__checkpoints_longorfs/", emit:LongOrfsCheckpointsFiles
    path "*.cmds", emit:longOrfsRootCmds
      
    """
    TransDecoder.LongOrfs -t ${psiclassTranscriptome}
    """
}

process eggnog_psiclass {
    publishDir "$params.outdir/04_assembly/psiclass/frameselection",  mode: 'copy'
    label 'process_medium' 

    	conda (params.enable_conda ? "bioconda::eggnog-mapper" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/eggnog-mapper:2.1.9--pyhdfd78af_0"
    } else {
        container "quay.io/biocontainers/eggnog-mapper:2.1.9--pyhdfd78af_0"
    }   
    
    input:
    path(psiclassPep)    

    output:
    path "*.hits", emit:eggnogBlastp 
   
    """
    emapper.py -i ${psiclassPep} --no_annot -o eggnog.blastp -m diamond --cpu ${task.cpus}

    """
}

process predict_psiclass {
    publishDir "$params.outdir/04_assembly/psiclass/frameselection",  mode: 'copy'
    label 'process_medium'

    	conda (params.enable_conda ? "bioconda::transdecoder" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/transdecoder:5.5.0--pl5321hdfd78af_5"
    } else {
        container "quay.io/biocontainers/transdecoder:5.5.0--pl5321hdfd78af_5"
    }   
    
    input:
    path(psiclassTranscriptomePredict) 
    path(eggnogBlastp)
    path(directory)
     
    output:
    path "${psiclassTranscriptomePredict}.transdecoder.pep", emit: transdecoderPEP
    path "${psiclassTranscriptomePredict}.transdecoder.bed", emit: transdecoderBED
    path "${psiclassTranscriptomePredict}.transdecoder.cds", emit: transdecoderCDS
    path "${psiclassTranscriptomePredict}.transdecoder.gff3", emit: transdecoderGFF3

      
    """
    TransDecoder.Predict -t ${psiclassTranscriptomePredict} --no_refine_starts --retain_blastp_hits ${eggnogBlastp}
    """
}

process genemodel_psiclass {
    publishDir "$params.outdir/06_predictions/psiclass",  mode: 'copy'
    label 'process_medium'

    	conda (params.enable_conda ? "bioconda::gmap" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/gmap:2021.08.25--pl5321h67092d7_2"
    } else {
        container "quay.io/biocontainers/gmap:2021.08.25--pl5321h67092d7_2"
    }      


    input:
    path(psiclassunfiltered)
    path(gmapIndex)

    output:
    path "transdecoder.psiclass.gff3", emit: unfiltered_genemodel
 
    script:
    """
    gmap -a 1 -D ${gmapIndex} -d gmap -f gff3_gene ${psiclassunfiltered} --fulllength --nthreads=${task.cpus} --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > transdecoder.psiclass.gff3 2> gmap.error
    """
}
process add_start_site {
    label 'process_medium'
    publishDir "$params.outdir/06_predictions/psiclass",  mode: 'copy'

     	conda (params.enable_conda ? "bioconda::agat" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0"
    } else {
        container "quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0"
    }          

    input:
    path(transdecoder)
    path(genome)

    output:
    path "gmap.psiclass.gff3", emit: trans_gmap
 
    script:
    """
    fold ${genome} > genome.fasta
    agat_sp_add_start_and_stop.pl --gff ${transdecoder} --fasta genome.fasta --out start_stop.gff
    awk -F'\t' -v OFS='\t' '{\$3=\$3}1' start_stop.gff | sed 's/mRNA/transcript/g' > gmap.psiclass.gff3
    """
}

process cluster_psiclass {
    publishDir "$params.outdir/04_assembly/psiclass/clustering",  mode: 'copy'
    label 'process_medium'
    module "${projectDir}/bin/usearch"
     
    // no available conda or singluarity 

    input:
    path(psiclasstransdecoderCDS)
    val(clust_id)

    output:
    path "centroids.cds", emit: centroids
    path "centroids.uc", emit: uc
 
    """
    usearch --cluster_fast ${psiclasstransdecoderCDS} --id ${clust_id} --centroids centroids.cds --uc centroids.uc 
    """

}
  

process getComplete_psiclass {
    publishDir "$params.outdir/04_assembly/psiclass/clustering",  mode: 'copy'
    label 'process_low'  

    	conda (params.enable_conda ? "bioconda::samtools=1.9" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/samtools:1.9--h91753b0_8"
    } else {
        container "quay.io/biocontainers/samtools:1.9--h91753b0_8"
    }   
     
    input:
    path(psiclassCentroids)

    output:
    path "psiclass.completeORF.cds", emit: completeORF
    path "id.txt", emit: completeOutput
 
    """
    grep "type:complete" ${psiclassCentroids} | awk '{print \$1}' > id.txt 
sed -i -e 's/>//g' id.txt
while read line;
	do samtools faidx ${psiclassCentroids} \$line >> psiclass.completeORF.cds;
done < id.txt
    """
}

process align_psiclass {
    publishDir "$params.outdir/04_assembly/psiclass",  mode: 'copy'
    label 'process_medium'

    	conda (params.enable_conda ? "bioconda::gmap" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/gmap:2021.08.25--pl5321h67092d7_2"
    } else {
        container "quay.io/biocontainers/gmap:2021.08.25--pl5321h67092d7_2"
    }  
     
    input:
    path(psiclassCompleteORF)
    path(gmapIndex)

    output:
    path "model.psiclass.gff3", emit: GeneModel
 
    """
    gmap -a 1 -D ${gmapIndex} -d gmap -f gff3_gene ${psiclassCompleteORF} --fulllength --nthreads=${task.cpus} --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > model.psiclass.gff3 2> gmap.error

    """
}