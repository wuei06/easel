process gmapIndex {
	publishDir "$params.outdir/02_index",  mode: 'copy'
	label 'process_low'
	
		conda (params.enable_conda ? "bioconda::gmap=2019.06.10" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/gmap:2019.06.10--pl526h2f06484_0"
    } else {
        container "quay.io/biocontainers/gmap:2019.06.10--pl526h2f06484_0"
    }
    	input:
    	path(genome)

    	output:
    	path "gmap/*" , emit: gmapindex
    

    	script:
    	"""
	gmap_build -D gmap -d gmap ${genome}

    	"""
}

process hisat2Index {
	publishDir "$params.outdir/02_index/hisat2",  mode: 'copy'
	label 'process_medium'

		conda (params.enable_conda ? "bioconda::hisat2=2.2.1" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/hisat2:2.2.1--py38he1b5a44_0"
    } else {
        container "quay.io/biocontainers/hisat2:2.2.1--py38he1b5a44_0"
    }

    	input:
    	path(genome)

    	output:
    	path "hisat2.index" , emit: hisat2index
    

    	script:
    	"""
    	hisat2-build -f ${genome} hisat2
		mkdir hisat2.index
		mv *.ht2 hisat2.index
    	"""
}


process hisat2Align {
	publishDir "$params.outdir/03_alignments/mapping_rates",  mode: 'copy', pattern: "*.txt"
	tag { id }
	label 'process_medium'	

		conda (params.enable_conda ? "bioconda::hisat2=2.2.1" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/hisat2:2.2.1--py38he1b5a44_0"
    } else {
        container "quay.io/biocontainers/hisat2:2.2.1--py38he1b5a44_0"
    }

    	input:
    	tuple val(id), path(trimmed)
		path(index)

    	output:
    	tuple val(id), path("*.sam"), emit: sam
		tuple val(id), path("*_mapping_rate.txt"), path("*.sam"), emit: sam_tuple
    

    	script:
    	""" 
        hisat2 -q -x ${index}/hisat2 -1 ${trimmed[0]} -2 ${trimmed[1]} -S ${id}.sam --summary-file ${id}_mapping_rate.txt
    	"""
}
process removing_samples {
	publishDir "$params.outdir/03_alignments/bam",  mode: 'copy', pattern: '*.bam'
	tag { id }	
	label 'process_single'

			conda (params.enable_conda ? "bioconda::samtools=1.9" : null)
    if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
        container "https://depot.galaxyproject.org/singularity/samtools:1.9--h91753b0_8"
    } else {
        container "quay.io/biocontainers/samtools:1.9--h91753b0_8"
    }

    	input:
    	tuple val(id), path(mapping_rate), path(sam)
		val(rate)

    	output:
		tuple val(id), path("*.bam"), optional:true, emit: pass_bam_tuple
		path("*.bam"), optional:true, emit: pass_bam
		path("*mr.txt"), emit: mr
    
    	script:
    	""" 
		rate=\$(awk '/overall alignment rate/ {gsub(/%/,"",\$1); print \$1}' ${mapping_rate})
		rate_var=\$(echo \$rate)

		if [[ "\${rate_var}" < "${rate}" ]]
		then
        echo "${id}'s mapping rate is less than ${rate}% and has been removed" > ${id}_mr.txt
		else
		samtools view -b -@ 16 ${sam} | samtools sort -o sorted_${id}.bam -@ 16
		echo "${id} \$rate_var" > ${id}_mr.txt
		fi 

    	"""
}
process log_mr {
	publishDir "$params.outdir/log",  mode: 'copy'
	label 'process_single'
   
    input:
	path(mr)
	val(rate)

	output:
	path("log_align.txt"), emit: log_align

    script:
    """ 
	header="library rate"
	echo "" > log_align.txt
	echo "##### Alignment Rates #####" >> log_align.txt

	mkdir bam
	mkdir removed
	mkdir kept
	mv ${mr} bam

	for f in bam/*
	do
	if grep -q 'and has been removed' \$f
	then
    mv \$f removed
	else
	mv \$f kept
	fi
	done

	if [ -z "\$(ls -A kept)" ]; then
    exit 1
	echo "All files were removed"
	else
    cat kept/* > kept.txt
	echo "Passed:" >> log_align.txt
	echo "\$header" >> log_align.txt
	cat kept.txt | while read line; do echo "\$line"; done >> log_align.txt
	fi

	if [ -z "\$(ls -A removed)" ]; then
	echo "" >> log_align.txt
	echo "Failed:" >> log_align.txt
    echo "None." >> log_align.txt
	echo "All libraries have a mapping rate greater than ${rate}%" >> log_align.txt
	else
    cat removed/* > removed.txt
	echo "" >> log_align.txt
	echo "Failed:" >> log_align.txt
	cat removed.txt | while read line; do echo "\$line"; done >> log_align.txt
	fi


    """
}