/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    VALIDATE INPUTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
// Check input path parameters to see if they exist
checkPathParamList = [
    params.genome
]
for (param in checkPathParamList) { if (param) { file(param, checkIfExists: true) } }
/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    IMPORT LOCAL MODULES/SUBWORKFLOWS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

include { data_input } from '../subworkflows/data_preparation.nf'
include { assembly_psiclass } from '../subworkflows/psiclass.nf'
include { assembly_stringtie2 } from '../subworkflows/stringtie2.nf'
include { orthodb_hints } from '../subworkflows/orthodb.nf'
include { stringtie2_psiclass } from '../subworkflows/filter_predictions.nf'
include { summary_stats } from '../subworkflows/quality_metrics.nf'
include { output } from '../subworkflows/log_easel.nf'
include { extrinsic_filter } from '../subworkflows/filter_extrinsic_evidence.nf'

workflow main_workflow {
	
	data_input 				( params.genome, params.scripts ) 
    assembly_stringtie2     ( data_input.out[0], data_input.out[2], params.augustus, params.bin, params.genome, data_input.out[3], params.configAugustus, params.configEST, params.configProtein )
	assembly_psiclass       ( data_input.out[1], data_input.out[2], params.augustus, params.bin, params.genome, data_input.out[3], params.configAugustus, params.configEST, params.configProtein )
	if(params.external_protein == 'true'){
	orthodb_hints			( params.augustus, params.bin, params.genome, data_input.out[3], params.configAugustus, params.configEST, params.configProtein, assembly_psiclass.out[5], assembly_stringtie2.out[5] )
	extrinsic_filter	 	( assembly_stringtie2.out[0], assembly_psiclass.out[0], params.genome, params.scripts, assembly_stringtie2.out[1], assembly_stringtie2.out[2], assembly_stringtie2.out[3], assembly_psiclass.out[1], assembly_psiclass.out[2], assembly_psiclass.out[3], orthodb_hints.out[0], orthodb_hints.out[1] )
	summary_stats		    ( extrinsic_filter.out[1], params.orthoDB, params.gFACs, extrinsic_filter.out[0], extrinsic_filter.out[2], params.genome, extrinsic_filter.out[3] )
	}
	else if(params.external_protein == 'false'){
	stringtie2_psiclass	 	( assembly_stringtie2.out[0], assembly_psiclass.out[0], params.genome, params.scripts, assembly_stringtie2.out[1], assembly_stringtie2.out[2], assembly_psiclass.out[1], assembly_psiclass.out[2] )
	summary_stats		    ( stringtie2_psiclass.out[1], params.orthoDB, params.gFACs, stringtie2_psiclass.out[0], stringtie2_psiclass.out[2], params.genome, stringtie2_psiclass.out[3] )
	}
	output					( data_input.out[4], data_input.out[5], data_input.out[6], assembly_stringtie2.out[4], assembly_psiclass.out[4], summary_stats.out[0], summary_stats.out[1] )
}

