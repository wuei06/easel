include { mergeGFF_orthodb; list_orthodb } from '../modules/combine_predictions.nf'
include { gtf } from '../modules/filtering/tasks_gff_to_gtf.nf'
include { filterSupport_orthodb } from '../modules/filtering/tasks_support.nf'
include { filterMulti; pythonMulti } from '../modules/filtering/tasks_multi_mono.nf' 
include { filterEggNOG; pythonEggNOG } from '../modules/filtering/tasks_gene_family.nf' 
include { filterLength; pythonLength } from '../modules/filtering/tasks_full_length.nf' 
include { filterSpliceSite; pythonSpliceSite } from '../modules/filtering/tasks_canonical_splice_sites.nf' 
include { filterStartSite; pythonStartSite } from '../modules/filtering/tasks_start_site_support.nf' 
include { filterRepeatCDS; pythonRepeatCDS } from '../modules/filtering/tasks_repeat_cds.nf' 
include { filterMap } from '../modules/filtering/tasks_map.nf' 
include { filterTranscript; pythonTranscriptLength; pythonMolecularWeight; filterGC } from '../modules/filtering/tasks_transcript.nf'
include { true_start_site; pad; split_fasta; rna_fold; merge; matrix } from '../modules/filtering/tasks_folding.nf' 
include { regressor; classifier; score; filtered; protein } from '../modules/filtering/tasks_random_forest.nf'

workflow extrinsic_filter {

    take:
    stringtie2_combined
    psiclass_combined
    genome
    scripts
    reference
    s_est
    s_prot
    s_trans
    p_est
    p_prot
    p_trans
    s_orthodb
    p_orthodb


    main:
    mergeGFF_orthodb	    ( stringtie2_combined.collect(), psiclass_combined.collect(), s_orthodb.collect(), p_orthodb.collect(), params.prefix )
    list_orthodb            ( s_est, s_prot, s_trans, p_est, p_prot, p_trans, s_orthodb, p_orthodb )
    gtf                     ( mergeGFF_orthodb.out.unfiltered_db )
    filterSupport_orthoDB   ( gtf.out.unfiltered_gtf, list.out.gff )
    filterMulti             ( gtf.out.unfiltered_gtf, list.out.gff, scripts, filterSupport_orthodb.out.support_db )
    pythonMulti             ( filterSupport_orthodb.out.support_db, scripts, filterMulti.out.txt)
    filterEggNOG            ( genome, gtf.out.unfiltered_gtf, scripts, filterSupport_orthodb.out.support_db )
    pythonEggNOG            ( filterSupport_orthodb.out.support_db, scripts, filterEggNOG.out.txt)
    filterLength            ( genome, gtf.out.unfiltered_gtf, list.out.gff, scripts, filterSupport_orthodb.out.support_db )
    pythonLength            ( filterSupport_orthodb.out.support_db, scripts, filterLength.out.txt)
    filterSpliceSite        ( genome, gtf.out.unfiltered_gtf, list.out.gff, scripts, filterSupport_orthodb.out.support_db )
    pythonSpliceSite        ( filterSupport_orthodb.out.support_db, scripts, filterSpliceSite.out.txt)
    filterStartSite         ( gtf.out.unfiltered_gtf, list.out.gff, scripts, filterSupport_orthodb.out.support_db, genome )
    pythonStartSite         ( filterSupport_orthodb.out.support_db, scripts, filterStartSite.out.txt)
    filterRepeatCDS         ( genome, gtf.out.unfiltered_gtf, pythonMulti.out.mono_multi )
    pythonRepeatCDS         ( filterRepeatCDS.out.cds, scripts, filterSupport_orthodb.out.support_db )
    filterTranscript        ( genome, gtf.out.unfiltered_gtf, pythonMulti.out.mono_multi )
    pythonTranscriptLength  ( filterTranscript.out.transcript, scripts, filterSupport_orthodb.out.support_db )
    pythonMolecularWeight   ( filterTranscript.out.transcript, scripts, filterSupport_orthodb.out.support_db )
    filterGC                ( filterTranscript.out.transcript, scripts, filterSupport_orthodb.out.support_db )
    true_start_site         ( mergeGFF.out.unfiltered )
    pad                     ( params.window, true_start_site.out.text, genome, scripts )
    split_fasta             ( pad.out.fasta, scripts, params.parts )
    fasta_ch = split_fasta.out.split.flatten().map { file -> tuple(file.baseName, file) }
    rna_fold                ( fasta_ch )
    merge                   ( rna_fold.out.rna_fold.collect() )
	matrix                  ( scripts, params.window, filterSupport_orthodb.out.support_db, merge.out.merge_rna )
    filterMap               ( matrix.out.gc_ratio.collect(), matrix.out.free_energy.collect(), filterGC.out.gc.collect(), pythonTranscriptLength.out.length.collect(), pythonMolecularWeight.out.weight.collect(), filterSupport_orthodb.out.support_db.collect(), pythonMulti.out.mono_multi.collect(), pythonEggNOG.out.eggnog.collect(), pythonLength.out.full_length.collect(), pythonSpliceSite.out.splice_site.collect(), pythonStartSite.out.start_site.collect(), pythonRepeatCDS.out.cds_repeat.collect(),pythonRepeatCDS.out.cds_length.collect())
    regressor               ( scripts, filterMap.out.features, params.training_set )
    classifier              ( scripts, filterMap.out.features, params.training_set )
    score                   ( scripts, filterMap.out.features )
    filtered                ( classifier.out.c_rf, regressor.out.r_rf, score.out.score, gtf.out.unfiltered_gtf )
    protein                 ( filtered.out.filtered_prediction, genome )

    emit:
    mergeGFF_orthodb.out.unfiltered_db
    filterEggNOG.out.protein
    filtered.out
    protein.out
    }