include { mergeGFF; list } from '../modules/combine_predictions.nf'
include { gtf } from '../modules/filtering/tasks_gff_to_gtf.nf'
include { filterSupport } from '../modules/filtering/tasks_support.nf'
include { filterMulti; pythonMulti } from '../modules/filtering/tasks_multi_mono.nf' 
include { filterEggNOG; pythonEggNOG } from '../modules/filtering/tasks_gene_family.nf' 
include { filterLength; pythonLength } from '../modules/filtering/tasks_full_length.nf' 
include { filterSpliceSite; pythonSpliceSite } from '../modules/filtering/tasks_canonical_splice_sites.nf' 
include { filterStartSite; pythonStartSite } from '../modules/filtering/tasks_start_site_support.nf' 
include { filterRepeatCDS; pythonRepeatCDS } from '../modules/filtering/tasks_repeat_cds.nf' 
include { filterMap } from '../modules/filtering/tasks_map.nf' 
include { filterTranscript; pythonTranscriptLength; pythonMolecularWeight; filterGC } from '../modules/filtering/tasks_transcript.nf'
include { true_start_site; pad; split_fasta; rna_fold; merge; matrix } from '../modules/filtering/tasks_folding.nf' 
include { regressor; classifier; score; filtered; protein } from '../modules/filtering/tasks_random_forest.nf'

workflow stringtie2_psiclass {

    take:
    stringtie2_combined
    psiclass_combined
    genome
    scripts
    s_est
    s_prot
    p_est
    p_prot

    main:
    mergeGFF		        ( stringtie2_combined.collect(), psiclass_combined.collect(), params.prefix )
    list                    ( s_est, s_prot, p_est, p_prot )       
    gtf                     ( mergeGFF.out.unfiltered )
    filterSupport           ( gtf.out.unfiltered_gtf, list.out.gff )
    filterMulti             ( gtf.out.unfiltered_gtf, list.out.gff, scripts, filterSupport.out.support )
    pythonMulti             ( filterSupport.out.support, scripts, filterMulti.out.txt)
    filterEggNOG            ( genome, gtf.out.unfiltered_gtf, scripts, filterSupport.out.support )
    pythonEggNOG            ( filterSupport.out.support, scripts, filterEggNOG.out.txt)
    filterLength            ( genome, gtf.out.unfiltered_gtf, list.out.gff, scripts, filterSupport.out.support )
    pythonLength            ( filterSupport.out.support, scripts, filterLength.out.txt)
    filterSpliceSite        ( genome, gtf.out.unfiltered_gtf, list.out.gff, scripts, filterSupport.out.support )
    pythonSpliceSite        ( filterSupport.out.support, scripts, filterSpliceSite.out.txt)
    filterStartSite         ( gtf.out.unfiltered_gtf, list.out.gff, scripts, filterSupport.out.support, genome )
    pythonStartSite         ( filterSupport.out.support, scripts, filterStartSite.out.txt)
    filterRepeatCDS         ( genome, gtf.out.unfiltered_gtf, pythonMulti.out.mono_multi )
    pythonRepeatCDS         ( filterRepeatCDS.out.cds, scripts, filterSupport.out.support )
    filterTranscript        ( genome, gtf.out.unfiltered_gtf, pythonMulti.out.mono_multi )
    pythonTranscriptLength  ( filterTranscript.out.transcript, scripts, filterSupport.out.support )
    pythonMolecularWeight   ( filterTranscript.out.transcript, scripts, filterSupport.out.support )
    filterGC                ( filterTranscript.out.transcript, scripts, filterSupport.out.support )
    true_start_site         ( mergeGFF.out.unfiltered )
    pad                     ( params.window, true_start_site.out.text, genome, scripts )
    split_fasta             ( pad.out.fasta, scripts, params.parts )
    fasta_ch = split_fasta.out.split.flatten().map { file -> tuple(file.baseName, file) }
    rna_fold                ( fasta_ch )
    merge                   ( rna_fold.out.rna_fold.collect() )
	matrix                  ( scripts, params.window, filterSupport.out.support, merge.out.merge_rna )
    filterMap               ( matrix.out.gc_ratio.collect(), matrix.out.free_energy.collect(), filterGC.out.gc.collect(), pythonTranscriptLength.out.length.collect(), pythonMolecularWeight.out.weight.collect(), filterSupport.out.support.collect(), pythonMulti.out.mono_multi.collect(), pythonEggNOG.out.eggnog.collect(), pythonLength.out.full_length.collect(), pythonSpliceSite.out.splice_site.collect(), pythonStartSite.out.start_site.collect(), pythonRepeatCDS.out.cds_repeat.collect(),pythonRepeatCDS.out.cds_length.collect())
    regressor               ( scripts, filterMap.out.features, params.training_set )
    classifier              ( scripts, filterMap.out.features, params.training_set )
    score                   ( scripts, filterMap.out.features )
    filtered                ( classifier.out.c_rf, regressor.out.r_rf, score.out.score, gtf.out.unfiltered_gtf )
    protein                 ( filtered.out.filtered_prediction, genome )

    emit:
    mergeGFF.out.unfiltered
    filterEggNOG.out.protein
    filtered.out
    protein.out
}