include { percent_masked; split_genome } from '../modules/split_genome.nf'
include { fetch_rna; fastp; remove_low_qc; log_qc } from '../modules/rna_reads.nf'
include { gmapIndex; hisat2Index; hisat2Align; removing_samples; log_mr } from '../modules/alignments.nf'

workflow data_input {

    take:
    genome
    scripts
	
    main:
    percent_masked      ( genome )
	split_genome		( genome, scripts, params.bins )
    if(params.user_reads == 'false'){
    sra_ch = Channel.fromPath(params.sra).splitCsv().flatten()
    fetch_rna           ( sra_ch )
    fastp               ( fetch_rna.out.sra )
    }
    else if(params.sra == 'false'){
    read_pairs_ch = Channel.fromFilePairs(params.user_reads)
    fastp               ( read_pairs_ch )
    }
    remove_low_qc       ( fastp.out.trimmed_json, params.total_reads, params.mean_length )
    log_qc              ( remove_low_qc.out.qc.collect(), params.total_reads, params.mean_length )
    gmapIndex		    ( genome )
    hisat2Index		    ( genome )
    hisat2Align		    ( remove_low_qc.out.pass_fastq, hisat2Index.out.hisat2index )
    removing_samples    ( hisat2Align.out.sam_tuple, params.rate )
    log_mr              ( removing_samples.out.mr.collect(), params.rate )

    emit:
	removing_samples.out.pass_bam_tuple
    removing_samples.out.pass_bam
    gmapIndex.out
    split_genome.out
    percent_masked.out
    log_qc.out
    log_mr.out
	
}