include { stringtie2; merge_stringtie2; orf_stringtie2; eggnog_stringtie2; predict_stringtie2; genemodel_stringtie2; add_start_site; cluster_stringtie2; getComplete_stringtie2; align_stringtie2 } from '../modules/stringtie2_gene_model.nf'
include { ESThints_stringtie2; proteinHints_stringtie2; trainingSet_stringtie2; augustusEST_stringtie2; augustusProtein_stringtie2; combineEST_stringtie2; combineProtein_stringtie2; combineAll_stringtie2 } from '../modules/stringtie2_prediction.nf'

workflow assembly_stringtie2 {
    take:
	hisat_alignments
	gmap_alignments
	script
	bin
	genome
	split
	augustus_config
	configEST
	configProtein

    main:
	
	stringtie2	                ( hisat_alignments )
	list_ch = stringtie2.out.stringtie2_gtf.collect()
	merge_stringtie2            ( list_ch, genome )
    orf_stringtie2         	    ( merge_stringtie2.out.fasta )
    eggnog_stringtie2           ( orf_stringtie2.out.longOrfsPepFiles )
    predict_stringtie2 	        ( merge_stringtie2.out.fasta, eggnog_stringtie2.out.eggnogBlastp, orf_stringtie2.out.LongOrfsDirFiles )
	genemodel_stringtie2        ( predict_stringtie2.out.transdecoderCDS, gmap_alignments )
    add_start_site				( genemodel_stringtie2.out.unfiltered_genemodel, genome)
	cluster_stringtie2 	        ( predict_stringtie2.out.transdecoderCDS, params.cluster_id )
    getComplete_stringtie2 	    ( cluster_stringtie2.out.centroids )
	align_stringtie2 		    ( getComplete_stringtie2.out.completeORF, gmap_alignments )
	ESThints_stringtie2 	    ( gmap_alignments, predict_stringtie2.out.transdecoderCDS, script )
	proteinHints_stringtie2	    ( script, genome, predict_stringtie2.out.transdecoderPEP, bin, params.miniprot, params.N, params.outn, params.max_intron )
	genome_ch = split.flatten().map { file -> tuple(file.baseName, file) }
	trainingSet_stringtie2		( align_stringtie2.out.GeneModel, genome, augustus_config, script, bin, params.prefix, params.test, params.train )
	augustusEST_stringtie2 	    ( trainingSet_stringtie2.out.gb, ESThints_stringtie2.out.stringtie2_est, script, configEST, augustus_config, bin, genome_ch, params.prefix )
	augustusProtein_stringtie2  ( trainingSet_stringtie2.out.gb, proteinHints_stringtie2.out.stringtie2_protein, script, configProtein, augustus_config, bin, genome_ch, params.prefix )
	est_ch = augustusEST_stringtie2.out.stringtie2_estHints.collect()
	combineEST_stringtie2       ( est_ch )
	protein_ch = augustusProtein_stringtie2.out.stringtie2_proteinHints.collect()
	combineProtein_stringtie2   ( protein_ch )
	combineAll_stringtie2       ( combineProtein_stringtie2.out.stringtie_proteinHints.collect(), combineEST_stringtie2.out.estGFF.collect() )

    emit:
	combineAll_stringtie2.out
	combineEST_stringtie2.out
	combineProtein_stringtie2.out
	add_start_site.out
	merge_stringtie2.out.log_stringtie
	trainingSet_stringtie2.out
}