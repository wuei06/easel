include { psiclass; orf_psiclass; eggnog_psiclass; predict_psiclass; genemodel_psiclass; add_start_site; cluster_psiclass; getComplete_psiclass; align_psiclass } from '../modules/psiclass_gene_model.nf'
include { ESThints_psiclass; proteinHints_psiclass; trainingSet_psiclass; augustusEST_psiclass; augustusProtein_psiclass; combineEST_psiclass; combineProtein_psiclass; combineAll_psiclass } from '../modules/psiclass_prediction.nf'

workflow assembly_psiclass {
    take:
    bam
	gmap_alignments
	script
	bin
	genome
	split
	augustus_config
	configEST
	configProtein

    main:
	list_ch = bam.toList()
	psiclass	                ( list_ch, genome )
    orf_psiclass         	    ( psiclass.out.fasta )
    eggnog_psiclass  	        ( orf_psiclass.out.longOrfsPepFiles )
    predict_psiclass 	        ( psiclass.out.fasta, eggnog_psiclass.out.eggnogBlastp, orf_psiclass.out.LongOrfsDirFiles )
	genemodel_psiclass          ( predict_psiclass.out.transdecoderCDS, gmap_alignments )
    add_start_site				( genemodel_psiclass.out.unfiltered_genemodel, genome)
	cluster_psiclass 	        ( predict_psiclass.out.transdecoderCDS, params.cluster_id )
    getComplete_psiclass 	    ( cluster_psiclass.out.centroids )
	align_psiclass 		        ( getComplete_psiclass.out.completeORF, gmap_alignments )
    ESThints_psiclass 	        ( gmap_alignments, predict_psiclass.out.transdecoderCDS, script )
	proteinHints_psiclass	    ( script, genome, predict_psiclass.out.transdecoderPEP, bin, params.miniprot, params.N, params.outn, params.max_intron )
	genome_ch = split.flatten().map { file -> tuple(file.baseName, file) }
	trainingSet_psiclass	    ( align_psiclass.out.GeneModel, genome, augustus_config, script, bin, params.prefix, params.test, params.train )
	augustusEST_psiclass 	    ( trainingSet_psiclass.out.gb, ESThints_psiclass.out.psiclass_est, script, configEST, augustus_config, bin, genome_ch, params.prefix )
	augustusProtein_psiclass    ( trainingSet_psiclass.out.gb, proteinHints_psiclass.out.psiclass_protein, script, configProtein, augustus_config, bin, genome_ch, params.prefix )
	est_ch = augustusEST_psiclass.out.psiclass_estHints.collect()
	combineEST_psiclass         ( est_ch )
	protein_ch = augustusProtein_psiclass.out.psiclass_proteinHints.collect()
	combineProtein_psiclass     ( protein_ch )
	combineAll_psiclass         ( combineProtein_psiclass.out.psiclass_proteinHints.collect(), combineEST_psiclass.out.estGFF.collect() )

	emit:
	combineAll_psiclass.out
	combineEST_psiclass.out
	combineProtein_psiclass.out
	add_start_site.out
	psiclass.out.log_psiclass
	trainingSet_psiclass.out
	
}