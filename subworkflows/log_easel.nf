include { easel_output } from '../modules/log.nf'

workflow output {

    take:
    genome
    rna_reads
    alignments
    stringtie2
    psiclass
    unfiltered
    filtered
	
    main:
    easel_output    ( genome, rna_reads, alignments, stringtie2, psiclass, unfiltered, filtered )
	
}