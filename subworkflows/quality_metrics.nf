include { busco_unfiltered; busco_filtered; gfacs_unfiltered; gfacs_filtered; entap_config; entap_unfiltered; entap_filtered; unfiltered_output; filtered_output } from '../modules/metrics.nf'

workflow summary_stats {

    take:
    protein
    odb
    gfacs
    unfiltered_gff
    filtered_gtf
    genome
    protein_filtered
    
    main:
	busco_unfiltered         ( protein, odb )
    gfacs_unfiltered         ( gfacs, unfiltered_gff, params.prefix )
    entap_config             ( params.taxon, params.tcoverage, params.qcoverage )
    entap_unfiltered         ( protein, entap_config.out.config, params.entap )
    unfiltered_output        ( busco_unfiltered.out.busco_unfiltered_txt, gfacs_unfiltered.out.unfiltered_stats, entap_unfiltered.out.entap_unfiltered_log)

    busco_filtered           ( protein_filtered, odb )
    gfacs_filtered           ( gfacs, filtered_gtf, params.prefix )
    entap_filtered           ( protein_filtered, entap_config.out.config, params.entap )
    filtered_output          ( busco_filtered.out.busco_filtered_txt, gfacs_filtered.out.filtered_stats, entap_filtered.out.entap_filtered_log)

    emit:
    unfiltered_output.out
    filtered_output.out
}