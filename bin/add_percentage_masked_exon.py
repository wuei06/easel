from Bio import SeqIO
import pandas as pd
import os
import sys
import re

filepath = sys.argv[1]
seq_objects = SeqIO.parse(filepath,'fasta')

sequences=[]
#load sequences into block container
for seq in seq_objects:
		sequences.append(seq)

seq_ids=[]
seq_lengths=[]
seq_count1=[]
seq_count2=[]
seq_masked=[]
seq_genes=[]
for record in sequences:
		seq_id=record.id
		seq_description=record.description
		fields=dict(re.findall(r'(\w+)=(.*?) ', seq_description))
		seq_gene=fields['gene']
		sequence=record.seq
		length=len(sequence)
		seq_genes.append(seq_gene)
		seq_ids.append(seq_id)
		seq_lengths.append(length)
		count1=0
		count2=0
		for i in sequence:
			if(i.islower()):
				count1=count1+1
			elif(i.isupper()):
				count2=count2+1
		seq_count1.append(count1)
		seq_count2.append(count2)
		masked=(count1/length*100)
		seq_masked.append(masked)

dataframe=pd.DataFrame()
dataframe['Transcript_ID']=seq_ids
dataframe['Gene_ID']=seq_genes
dataframe['Seq_Length']=seq_lengths
dataframe['Lowercase']=seq_count1
dataframe['Uppercase']=seq_count2
dataframe['Percent_Masked']=seq_masked

dataframe.to_csv('masked.tracking', sep='\t', encoding='utf-8', index=False)
