from Bio import SeqIO
import pandas as pd
import os
import sys
import re
from Bio.SeqUtils import molecular_weight

filepath = sys.argv[1]
seq_objects = SeqIO.parse(filepath,'fasta')

sequences=[]
#load sequences into block container
for seq in seq_objects:
		sequences.append(seq)

seq_ids=[]
seq_lengths=[]
seq_genes=[]
seq_weight=[]
for record in sequences:
		seq_id=record.id
		seq_description=record.description
		fields=dict(re.findall(r'(\w+)=(.*?) ', seq_description))
		seq_gene=fields['gene']
		sequence=record.seq
		length=len(sequence)
		seq_genes.append(seq_gene)
		seq_ids.append(seq_id)
		seq_lengths.append(length)
		try:
			weight=("%0.2f" % molecular_weight(record.seq))
			seq_weight.append(weight)
		except:
			seq_weight.append("NaN")

dataframe=pd.DataFrame()
dataframe['Transcript_ID']=seq_ids
dataframe['Gene_ID']=seq_genes
dataframe['Seq_Length']=seq_lengths
dataframe['Molecular_Weight']=seq_weight

dataframe.to_csv('masked.tracking', sep='\t', encoding='utf-8', index=False)
