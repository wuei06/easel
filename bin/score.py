import pandas as pd
import os
import re
import sys

features=sys.argv[1]
df = pd.read_csv(features, delimiter=r"\s+")

def calculate_score(x):
    score = 0
    if x['Full_Length'] == 1: 
        score += 1
    else:
        score += 0

    if x['EggNOG'] == 1 : 
        score += 1
    else:
        score += 0

    if x['CDS_Repeat_Content'] <= 10 : 
        score += 1
    else:
        score += 0

    if x['Canonical_Splice_Site'] == 1 : 
        score += 1
    else:
        score += 0

    if 35 <= x['GC_Content'] <= 45 :
        score += 1
    else:
        score += 0
    
    if x['Support'] > 3 :
        score += 1
    else:
        score += 0
    
    if x['Length'] >= 500 :
        score += 1
    else:
        score += 0

    return score

df['Score'] = df.apply(calculate_score, axis=1)
df.to_csv('score.tracking', sep=',', encoding='utf-8', index=False)    