import pandas as pd
import numpy as np
import sys

matrix = sys.argv[1]
feature = sys.argv[2]

df1 = pd.read_csv(matrix, delimiter='\t') 
df2 = pd.read_csv(feature, delimiter="\t")
mapping = dict(df2[['Transcript', 'Hit']].values)
df1['Canonical_Splice_Site'] = df1.Transcript.map(mapping)
df1.to_csv('feature.tracking', sep='\t', encoding='utf-8', index=False)
