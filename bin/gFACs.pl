#!/usr/bin/env perl
######################################################################################################################################################################
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# 2018 Madison Caballero
#    This file is part of gFACs.
#
#    gFACs is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    gFACs is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with gFACs.  If not, see <https://www.gnu.org/licenses/>.
######################################################################################################################################################################
######################################################################################################################################################################
$datestring = localtime();
$version = "1.1.3 (08/12/2021)";
######################################################################################################################################################################
#	gFACs - Gene filtering, analysis, and conversion. 
#	Madison.Caballero@uconn.edu
#
#	About this script:
#		This script is designed to provide statistics based off annotation outputs such as .gft, .gff3, .gff2, and .gff3 files. To do this, the input 
#		script is evaluated and provided the proper supporting script to create accurate statistics. 
#
######################################################################################################################################################################
######################################################################################################################################################################
$INPUT = join " ", @ARGV;									# This is the command
$infile = scalar @ARGV - 1;									# Infile is last in command
$output = scalar @ARGV - 2;									# Output location is second to last in command
$unique_done = 0;										# Value set
######################################################################################################################################################################
######################################################################################################################################################################
# THIS IS THE MANUAL that prints to command line
if (scalar @ARGV == 0){
	print 
		"gFACs\.\pl\n",
		"Contact\:\tMadison.Caballero\@uconn\.edu\n\n",
	        "Version: ", $version, "\n",
		"-------------------------------------------------------------GFACS MANUAL-------------------------------------------------------------\n\n",
		"   Welcome to gFACs, the one stop destination to understand what your annotation or alignment is secretly telling you. The script gFACs.pl \n",
		"is a task-assigning script that calls upon many different scripts to analyze your .gff3, .gff, or .gtf files by creating a median format \n",
		"called gene_table.txt. From here, statisitcs can be calculated and distribution tables created. Mutliple filter paramteters can be used to \n",
		"create an annotation or alignment you can be proud of.",
		"\n\n",

		"To run this script\:\n",
		"\tgFACs\.pl \-f \<format\> \[options\] \-O \<output directory\> \<input\_file\>\n\n",

		"Mandatory inputs\:\n",
		"\t\-f \<format\>\t\t\n",
		"\t\-O \<output directory\>\t\n",
		"\t\<input\_file\>\t .gff3, .gtf, or .gff as the LAST argument.\n",		

		"\nDefault output files:\n",
		"\tgFACs_log.txt\n",
		"\tgene_table.txt\n",

		"\n\nSupported formats\:\n\n",
	
		"\t\-f \[format\]\t",
		"Specifying a format\: A mandatory step to call upon the right script.\n",	
		"\n\tAvailable formats\:\n",

		"\t\tBraker format augustus.gff/gff3/gft:\n",	
		"\t\t\tbraker_2.0_gtf\n",						# augustus version 3.2.3 gft for braker	
		"\t\t\tbraker_2.0_gff\n",	 					# augustus version 3.2.3 gff for braker
		"\t\t\tbraker_2.0_gff3\n",                                             # augustus version 3.2.3 gff3 for braker
		"\t\t\tbraker_2.05_gff3\n",						# braker version 2.0 gff3
		"\t\t\tbraker_2.05_gff\n",						# braker_2.0_gff
		"\t\t\tbraker_2.05_gtf\n",						# braker_2.0_gtf
		"\t\t\tbraker_2.1.2_gtf - Works for 2.1.0-2.1.5\n",			# brker_2.1.2_gtf
		"\t\t\tbraker_2.1.2_gff3 - Works for 2.1.0-2.1.5\n\n",			# braker_2.1.2_gff3

		"\t\tBraker format braker.gtf/gff3:\n",
		"\t\t\tbraker_2.1.5_gtf\n",
		"\t\t\tbraker_2.1.5_gff3\n\n",

		"\t\tmaker_2.31.9_gff\n",						# maker 2.31.9 gff
		"\t\tprokka_1.11_gff\n",
                "\t\tgmap_2017_03_17_gff3\n",                                         # gmap version 2017/03/17 gff3 for braker
		"\t\tgenomethreader_1.6.6_gff3\n",					# genomethreader_1.6.6_gff3
		"\t\tstringtie_1.3.4_gtf\n",
		"\t\tgffread_0.9.12_gff3\n",						# gffread_0.9.12_gff3
		"\t\tgffcompare_0.10.4_gtf\n",
		"\t\texonerate_2.4.0_gff\n",						# exonerate_2.4.0_gff
		"\t\tEVM_1.1.1_gff3\n",
		"\t\tCoGe_1.0_gff\n",
		"\t\tPsiCLASS_1.0.2_gtf\n",
		"\t\tgFACs_gene_table\n",
		"\t\tgFACs_gtf\n",
		"\t\trefseq_gff\n",							# GFF / GFF3

#################

		"\n\nAdditional parameters you can include\:\n\n",
		"Flag descriptions: https://gfacs.readthedocs.io/en/latest/\n\n",

######FLAGS######

		"\t-p [prefix]",
		"\n\n",
		"\t--false-run",
		"\n",
		"\t\--statistics",
		"\n",
		"\t--statistics-at-every-step",
		"\n",
		"\t\-\-no-processing",
		"\n",
		"\t--no-gene-redefining",
		"\n",
		"\t--rem-5prime-incompletes",
		"\n",
		"\t--rem-3prime-incompletes",
		"\n",
		"\t--rem-5prime-3prime-incompletes",
		"\n",
		"\t--rem-all-incompletes",
		"\n",
		"\t\-\-rem\-monoexonics",
		"\n",
		"\t--rem-multiexonics",
		"\n",
		"\t--min-exon-size [nt number]",
		"\n",
		"\t--min-intron-size [nt number]",
                "\n",
		"\t--min-CDS-size [nt number]",
		"\n",
		"\t--unique-genes-only",
                "\n",
		"\t--sort-by-chromosome",
		"\n",

	"\nFlags that require that you input an entap tsv file:\n\n",
		
		"\t--entap-annotation [/path/to/your/entap/annotation.tsv]",
		"\n",
		"\tFlags you can include once entap table is specified:",
		"\n",
		"\t\t--annotated-ss-genes-only",
		"\n",
		"\t\t--annotated-all-genes-only\t",
                "\n",

	"\nFlags that require that you input a fasta file because seqeunce is being involved:",
		"\n\*\*\*NOTE: DO NOT RUN FASTA FLAGS WITH --no-processing. Genetic coordinates may disrupt bioperl commands!\*\*\*",
		"\n\n",

		"\t--fasta \[/path/to/your/nucleotide/fasta.fasta\]",
		"\n",

	"\tFlags you can include once fasta is specified:\n\n",

		"\t\t--splice-table\t\t",		
		"\n",
		"\t\t--canonical-only\t",
		"\n",
		"\t\t--rem-genes-without-start-codon",
		"\n",
		"\t\t--allow-alternate-starts".
		 "\n",
		"\t\t--rem-genes-without-stop-codon",
		"\n",
		"\t\t--rem-genes-without-start-and-stop-codon",
		"\n",
		"\t\t--allowed-inframe-stop-codons [number]",
                "\n",
		"\t\t--nt-content",
		"\n",
		"\t\t--get-fasta",
		"\n",
		"\t\t--get-protein-fasta",
		"\n",
		"\t\t--create-gtf",
		"\n",
		"\t\t--create-simple-gtf",
                "\n",
		"\t\t--create-gff3",
                "\n",

 "\nOutput compatibility options (requires fasta):",
		"\n",

		"\tThings compatible by default with --create-gtf : Jbrowse",

		"\n",
		"\t--compatibility \[option\] \[option\] etc\.\.\. ",
                "\n",
		"\t\tSnpEff",
                "\n",
		"\t\tEVM_1.1.1_gene_prediction\t",
                "\n",
                "\t\tEVM_1.1.1_alignment\t",
                "\n",
		"\t\tstringtie_1.3.6_gtf\t",
		"\n",
		"\t\thisat_2.1.0_gtf\t",
		"\n",
                "\t\tParsEval_gff3\t".
		"\n",

 "\nDistribution table creation:",
		"\n",
	
		"\t--distributions \[option\] \[option\] etc\.\.\.\t", 
		"\n",
		"\tAvailable distribution options are:",
		"\n",
		"\t\texon_lengths\t",
		"\n",
                "\t\tintron_lengths\t",
		"\n",
		"\t\tCDS_lengths\t",
                "\n",
                "\t\tgene_lengths\t",
                "\n",
		"\t\texon_position\t",
		"\n",
                "\t\texon_position_data\t",
                "\n",
		"\t\tintron_position\t",
		"\n",
		"\t\tintron_position_data\t",
                "\n",
	
		"\tAdvanced: To control step level on distributions after exon_lengths, intron_lengths, CDS_lengths, and gene_lengths can be added.",
		"\n\t\tExample: exon_lengths 10",

		"\n\n\n\#\#\#\n\n",
	
		"2018 Madison Caballero - University of Connecticut. This file is part of gFACs.\n",	
		"\tgFACs is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by\n",
	    	"\tthe Free Software Foundation, either version 3 of the License, or (at your option) any later version. gFACs is distributed in the hope that\n",
		"\tit will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n",
		"\tSee the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with gFACs.\n",
		"\tIf not, see <https://www.gnu.org/licenses/>.",

	"\n\n";
		die "\n\n";
} 
###############################################################################################################################################################
###############################################################################################################################################################
# ACTUAL gFACs stuff:
# Prefix

if ($INPUT =~ /\s\-p\s+(.+?)\s+/){							# Specifying where the prefix is predicted to be
	$prefix = $1;
	$pre = $prefix . "_";
}
$log = "$ARGV[$output]\/\/" . $pre . "gFACs_log.txt";					# Naming the log
open (LOG, ">$log");									# Creating the log > style
close LOG;										# Closing the > log style log

open (LOG, ">>$log");									# Reopening the log >> style ('tis better)
	print LOG "gFACs LOG", "\n\n", "Version\: ", $version, "\n";			# Print log and version
	print LOG "Time of run: ", $datestring, "\n\n";					# Time stamp
	print LOG "Command:", $0, " ", $INPUT, "\n\n"; 					# Command

if ($prefix =~ /./){print LOG "You have specified a prefix\:\t", $prefix, "\n\n";}	# Prefix note
if ($INPUT =~ /\-f\s*(.+)\s*/){ print LOG "You have specfied a format\:\t";		# Format specifications

#############FORMAT DECISIONS#########################################################################################################
	
	if ($1 =~ /gmap_2017_03_17_gff3/){						# gmap_2017_03_17_gff3
		print LOG "GMAP version 2017\/03\/17 in gff3 format\n";
		$format = "gmap_2017_03_17_gff3";		
	}
	if ($1 =~ /braker\_2\.05\_gtf/){                                             	# braker_2.05_gtf
                print LOG "BRAKER version 2\.05 in gtf format\n";
		$format = "braker\_2\.05\_gtf";		
        }
	if ($1 =~ /braker\_2\.05\_gff\s/){                                             	# braker_2.05_gff
                $format = "braker\_2\.05\_gff";		
    		print LOG "BRAKER version 2\.05 in gff format\n";
	}
	if ($1 =~ /braker\_2\.05\_gff3/){                                  		# braker_2.05_gff3
               	  print LOG "BRAKER version 2\.05 in gff3 format\n";
        	  $format = "braker\_2\.05\_gff3";
	}
        if ($1 =~ /braker\_2\.1.2\_gff3/){                                               # braker_2.05_gff3
                  print LOG "BRAKER version 2.1.2 in gff3 format\n";
                  $format = "braker\_2.1.2\_gff3";
        }
	if ($1 =~ /braker\_2\.0\_gff3/){						# braker_2.0_gff3
		print LOG "BRAKER version 2\.0 in gff3 format\n";
                $format = "braker\_2\.0\_gff3";
	}
	if ($1 =~ /augustus\_unknown\_gtf/){						# augustus unknown gtf
		print LOG "AUGUSTUS version UNKNOWN in gtf format\n";
                $format = "augustus\_unknown\_gtf";
	}
	if ($1 =~ /genomethreader\_1\.6\.6\_gff3/){					# genomethreader_1.6.6_gff3
		print LOG "GENOMETHREADER version 1.6.6 in gff3 format\n";
		$format = "genomethreader\_1\.6\.6\_gff3";
	}
	if($1 =~ /gffread\_0\.9\.12\_gff3/){						# gffread_0.9.12_gff3
		print LOG "GFFREAD version 0.9.12 in gff3 format\n";
		$format = "gffread\_0\.9\.12\_gff3";	
	}
        if($1 =~ /gffcompare\_0\.10\.4\_gtf/){                                            # gffread_0.9.12_gff3
                print LOG "GFFCOMPARE version 0.10.4 in gtf format\n";
                $format = "gffcompare_0.10.4_gtf";
        }
	if ($1 =~ /braker\_2\.0\_gff\s/){
		print LOG "BRAKER version 2\.0 in gff format\n";			# braker_2.0_gff
		$format = "braker_2\.0_gff";
	}
	if ($1 =~ /braker\_2\.0\_gtf\s/){						# braker_2.0_gtf
		print LOG "BRAKER version 2\.0 in gtf format\n";		
		$format = "braker\_2\.0\_gff";
	}
	if ($1 =~ /exonerate\_2\.4\.0\_gff/){						#exonerate_2.4.0_gff
		print LOG "EXONERATE version 2\.4\.0 in gff format\n";
		$format = "exonerate\_2\.4\.0\_gff";
	}
	if ($1 =~ /braker\_2\.1\.2\_gtf/){                                           
                print LOG "BRAKER version 2\.1\.2 in gtf format\n";
                $format = "braker\_2\.1\.2\_gtf";
	}
        if ($1 =~ /braker\_2\.1\.5\_gtf/){
                print LOG "BRAKER version 2\.1\.5 (braker.gtf) in gtf format\n";
                $format = "braker\_2\.1\.5\_gtf";
        }
        if ($1 =~ /braker\_2\.1\.5\_gff3/){
                print LOG "BRAKER version 2\.1\.5 (braker.gff3) in gff3 format\n";
                $format = "braker\_2\.1\.5\_gff3";
        }
	if ($1 =~ /maker\_2\.31\.9\_gff/){
		print LOG "MAKER version 2.31.9 in gff format\n";
		$format = "maker_2.31.9_gff";
	}
	if ($1 =~ /gFACs_gene_table/){
		print LOG "GFACS all version in gene_table.txt format\n";
		$format = "gFACs_gene_table";
	}
	if ($1 =~ /gFACs_gtf/){
		print LOG "GFACS all version in out.gtf format\n";
		$format = "gFACs_gtf";
	}
	if ($1 =~ /EVM_1.1.1_gff3/){
		print LOG "EVIDENCE MODELER version 1.1.1 in gff3 format\n";
		$format = "EVM_1.1.1_gff3";
	}
	if ($1 =~ /refseq_gff/){
		print LOG "NCBI REFSEQ GFF/GFF3 file format\n";
		$format = "refseq_gff";
	}
	if ($1 =~ /genbank_gbff/){
		print LOG "NCBI GENBANK GBFF file format\n";
		$format = "genbank_gbff";
	}
	if ($1 =~ /prokka_1.11_gff/){
		print LOG "PROKKA version 1.11 in gff format. Assuming only monoexonics!!\n";
		$format = "prokka_1.11_gff";
	}
	if ($1 =~ /stringtie_1.3.4_gtf/){
                print LOG "STRINGTIE version 1.3.4 in gtf format\n";
                $format = "stringtie_1.3.4_gtf";
        }
	if ($1 =~ /CoGe_1.0_gff/){
		print LOG "CoGe version 1.0 in gff format\n";
		$format = "CoGe_1.0_gff";
	}
	if ($1 =~ /PsiCLASS_1.0.2_gtf/){
                print LOG "PsiCLASS version 1.0.2 in gtf format\n";
		$format = "psiclass_1.0.2_gtf";
	}
	##########################################################################################################################
}
if ($INPUT !~ /\-f\s*(.+)\s*/){									# No format error and die
	print LOG "You have not specified a format.";
	die "You must specify a format!";
}
if ($INPUT =~ /\-O\s*$ARGV[$output]/){									# Output destination set up
	if ($ARGV[$output] =~ /.+$/){ print LOG "\nYou have specified an output directory: $ARGV[$output]\n";}
}
if ($INPUT !~ /\-O\s*$ARGV[$output]/){die "You must specify an output directory using the -O command. It must also be before your input file.\n";} 

#LOG OF FORMAT###################################################################################################################################################
#COMMAND#########################################################################################################################################################
#CONVERSION TO GENE TABLE

$0 =~ s/(.+)gFACs.pl//;
$loc = $1;
$infile = scalar @ARGV - 1;									
$gene_table = $ARGV[$output] . "\/" . "\/" . $pre . "gene_table.txt";				# Create the gene table name

push @command, "perl ";							# MANDATORY
push @command, "$loc";
push @command, "format_scripts\/";					# MANDATORY --> LOCATION OF FORMAT SCRIPTS
push @command, $format;							# MANDATORY --> FORMAT
push @command, "\.pl ";							# MANDATORY
push @command, "$ARGV[$infile] ";					# MANDATORY --> INFILE
push @command, "$ARGV[$output] ";					# MANDATORY --> outdir
push @command, "$pre ";							# MANDATORY --> prefix

$command = join "", @command;
system "$command";							# Doing the format command

print LOG "\n", "Format command:\t", $command, "\n";			# PRINT TO LOG THE COMMAND

## THE OUTPUT gene_table.txt HAS BEEN CREATED AT THIS POINT at the specified location!

# If --false-run is activated, a false file is created and deleted at the end.
if ($INPUT =~ /\-\-false-run/){
 $tempgt = $ARGV[$output] . "\/" . "\/" . $pre . "temp_gene_table.txt";
 $string = "cp $gene_table $tempgt";
 system ($string);
}


###############################################################################################################################################################
## FLAG TO SCRIPT STATS

print LOG "\n\nFlags:\n\n";

if ($INPUT =~ /\-\-false-run/){
	 print LOG "--false-run has been activated. Filters will not be retained.", "\n\n"; 
}

if ($INPUT !~ /\-\-no-processing/){
	print LOG "--no-processing has NOT been activated. I will look for overlapping gene space and see if it is the result of multiple transcripts mapping to the same location. Splice variants will be labeled as [gene].1, [gene].2, etc... and added to the end of gene_table.txt.";

	$string = "perl $loc" . "task_scripts/overlapping_exons.pl $gene_table $ARGV[$output] $pre >> $log";
        print LOG "\n\tCommand: ", "$string", "\n";
	print LOG "Results:\n";

	system ($string);
	$name = $ARGV[$output] . "\/" . "\/" . $pre . "non_overlapping_gene_table.txt";
	$string = "mv $name $gene_table";
	system ($string);

	$string = "perl $loc" . "task_scripts/splice_variants.pl $gene_table $ARGV[$output] $pre >> $log";

	print LOG "\tCommand: ", $string, "\n";
        system ($string);
	
	$name = $ARGV[$output] . "\/" . "\/" . $pre . "transcripts.txt";
        system "cat $name >> $gene_table";
	system "rm $name";
	#$name = $ARGV[$output] . "\/" . "\/" . $pre . "non_overlapping_gene_table.txt";
	#system "rm $name";
	$name = $ARGV[$output] . "\/" . "\/" . $pre . "overlapping_gene_table.txt";
	system "rm $name";
	
	if ($INPUT =~ /\-\-statistics-at-every-step/){
	        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        system($string);
                        $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
                
		print LOG "\n";
	        system ($string);
	}
	if ($INPUT =~ /\-\-false-run/){
        	$string = "cp $tempgt $gene_table";
	        system ($string);
	}
}
#############################################################################################################################################################
#FORMAT CHECK
$string = "perl $loc" . "task_scripts/gene_table_fixer.pl $ARGV[$output] $pre";
$output_name = $ARGV[$output] . "\/" . $pre . "checked_gene_table.txt";

        print LOG "\nFormat Check!\n\tCommand: ", $string, "\n";
        system ($string);
        system "mv $output_name $gene_table";

	if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
#############################################################################################################################################################
#INCOMPLETENESS CHECK

if ($INPUT =~ /--no-gene-redefining/){
	print LOG "\nGenes will not be defined by exons but rather by provided gene boundaries. Incompletes are not labeled but can still be filtered.\n";
}

if ($INPUT !~ /--no-gene-redefining/){
	$string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
	$output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";

	 print LOG "\nIncompleteness Check! Genes will be labeled as 5' or 3' incompletes.\n\tCommand: ", $string, "\n";
         system ($string);
         system "mv $output_name $gene_table";

        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
}

#############################################################################################################################################################
if ($INPUT =~ /--rem-5prime-incompletes/){
	print LOG "\n--rem-5prime-incompletes has been activated. Genes that are incomplete at the 5' end are removed. Those that do not are kept in gene_table.txt.\n";
	$string = "perl $loc" . "task_scripts/remove_starting_introns.pl $gene_table $ARGV[$output] $pre >> $log";
	
	print LOG "\tCommand: ", $string, "\n";
	print LOG "Results:", "\n";
	system ($string);

	$name = $ARGV[$output] . "\/" . "\/" . $pre . "complete_genes.txt";
	system "mv $name $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";

		if ($INPUT =~ /--no-gene-redefining/){
		        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
			system($string);
			$output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
			$string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
		}

                print LOG "\n";
                system ($string);
        }
	        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
}
if ($INPUT =~ /--rem-3prime-incompletes/){
        print LOG "\n--rem-3prime-incompletes has been activated. Genes that are incomplete at 3' end are removed. Those that do not are kept in gene_table.txt.\n";
        $string = "perl $loc" . "task_scripts/remove_ending_introns.pl $gene_table $ARGV[$output] $pre >> $log";

        print LOG "\tCommand: ", $string, "\n";
        print LOG "Results:", "\n";
        system ($string);

	$name = $ARGV[$output] . "\/" . "\/" . $pre . "complete_genes.txt";
        system "mv $name $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";

		if ($INPUT =~ /--no-gene-redefining/){
                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        system($string);
                        $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
                }

                print LOG "\n";
                system ($string);
        }
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
}

if ($INPUT =~ /--rem-5prime-3prime-incompletes/){
        print LOG "\n--rem-5prime-3prime-incompletes has been activated. Genes that are simultaneously incomplete at the 5' end and 3' end are removed. Those that are complete or have only one end incomplete are kept in gene_table.txt.\n";
        $string = "perl $loc" . "task_scripts/remove_both_start_and_ending_introns.pl $gene_table $ARGV[$output] $pre >> $log";

        print LOG "\tCommand: ", $string, "\n";
        print LOG "Results:", "\n";
        system ($string);

        $name = $ARGV[$output] . "\/" . "\/" . $pre . "complete_genes.txt";
        system "mv $name $gene_table";

        if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";

                if ($INPUT =~ /--no-gene-redefining/){
                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        system($string);
                        $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
                }

                print LOG "\n";
                system ($string);
        }
                if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
}


if ($INPUT =~ /--rem-all-incompletes/){
	print LOG "\n--rem-all-incompletes has been activated. Those that are complete are kept in gene_table.txt.\n";
        $string = "perl $loc" . "task_scripts/remove_starting_introns.pl $gene_table $ARGV[$output] $pre >> $log";

        print LOG "\tCommand: ", $string, "\n";
        print LOG "Results:", "\n";
        system ($string);

        $name = $ARGV[$output] . "\/" . "\/" . $pre . "complete_genes.txt";
        system "mv $name $gene_table";

        if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
		if ($INPUT =~ /--no-gene-redefining/){
                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        system($string);
                        $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
                }

                print LOG "\n";
                system ($string);
        }
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
        $string = "perl $loc" . "task_scripts/remove_ending_introns.pl $gene_table $ARGV[$output] $pre >> $log";

        print LOG "\tCommand: ", $string, "\n";
        print LOG "Results:", "\n";
        system ($string);

        $name = $ARGV[$output] . "\/" . "\/" . $pre . "complete_genes.txt";
        system "mv $name $gene_table";

        if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
		if ($INPUT =~ /--no-gene-redefining/){
                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        system($string);
                        $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
                }
                print LOG "\n";
                system ($string);
        }
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
}
	
############################################################################################################################################################
if ($INPUT =~ /\-\-rem\-monoexonics/){
	print LOG "\n--rem-monoexonics has been activated. Genes that don't have an intron (2+ exons) will be removed from the gene_table.txt.\n";
	$string = "perl $loc" . "task_scripts/remove_monoexonics.pl $gene_table $ARGV[$output] $pre >> $log";
	print LOG "\tCommand: ", $string, "\n";
	print LOG "Results:", "\n";	

	system ($string);

	$multi = $ARGV[$output] . "\/" . "\/" . $pre . "multiexonic_genes.txt";
	system "mv $multi $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
		if ($INPUT =~ /--no-gene-redefining/){
                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        system($string);
                        $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
                }
                print LOG "\n";
                system ($string);
        }
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
}

if ($INPUT =~ /\-\-rem\-multiexonics/){
        print LOG "\n--rem-multiexonics has been activated. Genes that have an intron (2+ exons) will be removed from the gene_table.txt.\n";
        $string = "perl $loc" . "task_scripts/remove_multiexonics.pl $gene_table $ARGV[$output] $pre >> $log";
        print LOG "\tCommand: ", $string, "\n";
        print LOG "Results:", "\n";

        system ($string);

        $mono = $ARGV[$output] . "\/" . "\/" . $pre . "monoexonic_genes.txt";
        system "mv $mono $gene_table";

        if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
		if ($INPUT =~ /--no-gene-redefining/){
                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        system($string);
                        $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
                }

                print LOG "\n";
                system ($string);
        }
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
}
#############################################################################################################################################################
if ($INPUT =~ /\-\-min-exon-size/){
	if ($INPUT =~ /--min-exon-size\s+(\d+?)\s/){$min = $1;}
	else{$min = 20;}

	print LOG "\n--min-exon-size has been activated. Genes with an exon that is less than $min will be removed from gene_table.txt";
	$string = "perl $loc" . "task_scripts/minimum_exon.pl $gene_table $ARGV[$output] $min  $pre >> $log";
	print LOG "\n\tCommand: ", $string, "\n";
	print LOG "Results:\n";
	system ($string);

	$name = $ARGV[$output] . "\/" . "\/" . $pre . "minumum_exons_gene_table.txt";
	system "mv $name $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
		if ($INPUT =~ /--no-gene-redefining/){
                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        system($string);
                        $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
                }
                print LOG "\n";
                system ($string);
        }
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
}
if ($INPUT =~ /\-\-min-intron-size/){
        if ($INPUT =~ /--min-intron-size\s+(\d+?)\s/){$min = $1;}
        else{$min = 20;}

        print LOG "\n--min-intron-size has been activated. Genes with an intron that is less than $min will be removed from gene_table.txt";
        $string = "perl $loc" . "task_scripts/minimum_intron.pl $gene_table $ARGV[$output] $min $pre >> $log";
        print LOG "\n\tCommand: ", $string, "\n";
        print LOG "Results:\n";
        system ($string);

        $name = $ARGV[$output] . "\/" . "\/" . $pre . "minumum_introns_gene_table.txt";
        system "mv $name $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
		if ($INPUT =~ /--no-gene-redefining/){
                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        system($string);
                        $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
                }
                print LOG "\n";
                system ($string);
        }
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
}
if ($INPUT =~ /\-\-min-CDS-size/){
	if ($INPUT =~ /--min-CDS-size\s+(\d+?)\s/){$min = $1;}
        else{$min = 74;}

	print LOG "\n--min-CDS-size has been activated. Genes with a CDS that is less than $min will be removed from gene_table.txt";
        $string = "perl $loc" . "task_scripts/CDS_size.pl $gene_table $ARGV[$output] $min $pre >> $log";
        print LOG "\n\tCommand: ", $string, "\n";
        print LOG "Results:\n";
        system ($string);

	$name = $ARGV[$output] . "\/" . "\/" . $pre . "minumum_CDS_gene_table.txt";
        system "mv $name $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
		if ($INPUT =~ /--no-gene-redefining/){
                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        system($string);
                        $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
                }
                print LOG "\n";
                system ($string);
        }
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
}

if ($INPUT =~ /\-\-sort-by-chromosome/){
        print LOG "\n--sort-by-chromosome has been activated. Genes will be sorted by chromosome but this does not mean genes within the chromosome will be ordered by position. This will affect all outputs from here.";
        $string = "perl $loc" . "task_scripts/sort_by_chromosome.pl $gene_table $ARGV[$output] \. $pre >> $log";
        print LOG "\n\tCommand: ", $string, "\n";
        system ($string);
        $name = $ARGV[$output] . "\/" . "\/" . $pre . "sorted_gene_table.txt";
        system "mv $name $gene_table";
}

#############################################################################################################################################################
#############################################################################################################################################################
# ENTAP AREA

if ($INPUT =~ /--entap-annotation\s*.+\s/){
        print LOG "\nYou have specified an entap annotation! Good choice!\n";                                                     
                for ($a = 0; $a < scalar @ARGV; $a++){if ($ARGV[$a] =~ /--entap-annotation/){$entap = $ARGV[$a+1];}}
               
	if ($INPUT =~ /--annotated-all-genes-only/){                                                                           
		print LOG "\n--annotated-all-gene-only has been activated. Only genes that have been annotated by similarity search or EggNOG hit will be kept. Passed genes will be found in gene_table.txt\n";
		$string = "perl $loc" . "task_scripts/annotated_all_genes_only.pl $entap $ARGV[$output] $pre >> $log";
		print LOG "\tCommand: ", $string, "\n";
		print LOG "Results:\n";
		system ($string);
	
		$name = $ARGV[$output] . "\/" . "\/" . $pre . "annotated_gene_table.txt";
	        system "mv $name $gene_table";

		if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
			if ($INPUT =~ /--no-gene-redefining/){
                  	      $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        	system($string);
        	                $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
	                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
	                }
                        print LOG "\n";
                        system ($string);
                }
	}
	        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
	 if ($INPUT =~ /--annotated-ss-genes-only/){
                print LOG "\n--annotated-all-genes-only has been activated. Only genes that have been annotated by similarity search will be kept. Passed genes will be found in gene_table.txt\n";
                $string = "perl $loc" . "task_scripts/annotated_ss_genes_only.pl $entap $ARGV[$output] $pre >> $log";
		print LOG "\tCommand: ", $string, "\n";
                print LOG "Results:\n";
                system ($string);

                $name = $ARGV[$output] . "\/" . "\/" . $pre . "annotated_gene_table.txt";
                system "mv $name $gene_table";

                if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
			if ($INPUT =~ /--no-gene-redefining/){
	                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
        	                system($string);
                        	$output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                	        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
        	        }
	
                        print LOG "\n";
                        system ($string);
                }
        }
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }

}
#############################################################################################################################################################
#############################################################################################################################################################
# FASTA FANCY
#
	use Bio::Index::Fasta;
        use Bio::Seq;
if ($INPUT !~ /--fasta\s*.+\.fa/){
	print LOG "\nNO FASTA OR NO VALID FASTA HAS BEEN SPECIFIED \-\- all fasta-required commands cannot be executed. If you did specify a fasta,\n",
		"\tmake sure it ends in .fa or .fasta. See manual for more details on fasta commands and specifications!\n";
}
if ($INPUT =~ /--fasta\s*.+\.fasta/ or $INPUT =~ /--fasta\s*.+\.fa/){
	print LOG "\nYou have specified a fasta! Indexing it...\n";							# INDEXING FASTA
		for ($a = 0; $a < scalar @ARGV; $a++){if ($ARGV[$a] =~ /--fasta/){$fasta = $ARGV[$a+1];}}
		$file_name = $fasta . ".idx";										# Creating Index file name

		$string = "perl $loc" . "task_scripts/index.pl $fasta >> $log";
		system ($string);	

	 print LOG "\nChecking the fasta for annotation scaffold match...\n"; 
		 $string = "perl $loc" . "task_scripts/genome_check.pl $fasta $ARGV[$output] $pre >> $log";
		 print LOG "\tCommand: ", $string, "\n";
                 system ($string);

                $kill = $ARGV[$output] . "\/" . "\/" . $pre . "kill_command.txt";
		open(KILL, $kill);
		@kill_com = <KILL>;
		$kill_com = join "", @kill_com;
		close KILL;
		
		system("rm $kill");
		if($kill_com == 1){ die "Fatal error. See run log. Died";}
		if($kill_com == 0){ print LOG "\t...looks good!\n";} 


	if ($INPUT =~ /\-\-canonical-only\s/){
		print LOG "\n--canonical-only has been activated. Genes without canonical gt-ag splice sites are filterd out. Monoexonics remain if not removed with flag.\n";
		$string = "perl $loc" . "task_scripts/canonical_only.pl $fasta $ARGV[$output] $pre >> $log";
		print LOG "\tCommand: ", $string, "\n";
		print LOG "Results:\n";
		system ($string);

		$canonical = $ARGV[$output] . "\/" . "\/" . $pre . "canonical_genes.txt";
		system "mv $canonical $gene_table";

		if ($INPUT =~ /\-\-statistics-at-every-step/){
                	$statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
        	        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
			if ($INPUT =~ /--no-gene-redefining/){
        	                $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
	                        system($string);
                        	$output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                	        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
        	        }
	
	                print LOG "\n";
                	system ($string);
        	}
	}
	if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
	if ($INPUT =~ /--rem-genes-without-start-codon/){
		print LOG "\n--rem-genes-without-start-codon has been activated. Genes that have a start codon are kept in gene_table.txt.\n";
		$string = "perl $loc" . "task_scripts/rem_genes_without_start.pl $fasta $ARGV[$output] $pre >> $log";

		if ($INPUT =~ /--allow-alternate-starts/){
			 print LOG "\n--allow-alternate-starts been activated. Genes that have an ATG/GTG/TTG start codon are kept in gene_table.txt.\n";
			 $string = "perl $loc" . "task_scripts/rem_genes_without_start_alternate.pl $fasta $ARGV[$output] $pre >> $log";
		}
		print LOG "\tCommand: ", $string, "\n";
		print LOG "Results:\n";
		system ($string);
		$name = $ARGV[$output] . "\/" . "\/" . $pre . "start_and_stop_gene_table.txt";
		$string = "mv $name $gene_table";
		system ($string);

		if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
			if ($INPUT =~ /--no-gene-redefining/){
	                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        	system($string);
                	        $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
        	                $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
	                }

                        print LOG "\n";
                        system ($string);
                }

	}
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
	if ($INPUT =~ /--rem-genes-without-stop-codon/){
                print LOG "\n--rem-genes-without-stop-codon has been activated. Genes that have a stop codon are kept in gene_table.txt.\n";
                $string = "perl $loc" . "task_scripts/rem_genes_without_end.pl $fasta $ARGV[$output] $pre >> $log";
                print LOG "\tCommand: ", $string, "\n";
                print LOG "Results:\n";
                system ($string);
                $name = $ARGV[$output] . "\/" . "\/" . $pre . "start_and_stop_gene_table.txt";
                $string = "mv $name $gene_table";
                system ($string);

		print LOG "\nPerforming a frame check to guarentee stop codon is in frame. Genes that pass are kept in gene_table.txt.\n";
                $string = "perl $loc" . "task_scripts/frame_detection.pl $gene_table $ARGV[$output] $pre >> $log";
                print LOG "\tCommand: ", $string, "\n";
                print LOG "Results:\n";
                system ($string);

                $file_name = $ARGV[$output] . "\/" . "\/" . $pre . "frame_checked_gene_table.txt";
                system "mv $file_name $gene_table";

		if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
			if ($INPUT =~ /--no-gene-redefining/){
	                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        	system($string);
                	        $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
        	                $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
	                }

                        print LOG "\n";
                        system ($string);
                }
        }
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
       if ($INPUT =~ /--rem-genes-without-start-and-stop-codon/){
                print LOG "\n--rem-genes-without-start-and-stop-codon has been activated. Genes that have either a stop or start codon are kept in gene_table.txt.\n";
                $string = "perl $loc" . "task_scripts/rem_genes_without_start_and_end.pl $fasta $ARGV[$output] $pre >> $log";

		if ($INPUT =~ /--allow-alternate-starts/){
                         print LOG "\n--allow-alternate-starts been activated. Genes that have an ATG/GTG/TTG start codon are kept in gene_table.txt.\n";
                         $string = "perl $loc" . "task_scripts/rem_genes_without_start_and_end_alternate.pl $fasta $ARGV[$output] $pre >> $log";
                }

                print LOG "\tCommand: ", $string, "\n";
                print LOG "Results:\n";
                system ($string);
                $name = $ARGV[$output] . "\/" . "\/" . $pre . "start_and_stop_gene_table.txt";
                $string = "mv $name $gene_table";
                system ($string);

                print LOG "\nFrame checked in script";

		if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                        if ($INPUT =~ /--no-gene-redefining/){
                                $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                                system($string);
                                $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                                $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
                        }

                        print LOG "\n";
                        system ($string);
                }
        }
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
	if ($INPUT =~ /--allowed-inframe-stop-codons/){
		if ($INPUT =~ /--allowed-inframe-stop-codons\s+(\d+?)\s/){$min = $1;}
	        else{$min = 0;}

	        print LOG "\n--allowed-inframe-stop-codons has been activated. Genes with more than $min inframe stop codons will be removed from gene_table.txt";
        	$string = "perl $loc" . "task_scripts/inframe_stop.pl $fasta $ARGV[$output] $min $pre >> $log";
	        print LOG "\n\tCommand: ", $string, "\n";
        	print LOG "Results:\n";
	        system ($string);

        	$name = $ARGV[$output] . "\/" . "\/" . $pre . "inframe_stop_filtered_gene_table.txt";
	        system "mv $name $gene_table";
	
		if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
			if ($INPUT =~ /--no-gene-redefining/){
        	                $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
	                        system($string);
                        	$output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                	        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
        	        }
	
                        print LOG "\n";
                        system ($string);
                }
	}
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
	if ($INPUT =~ /--unique-genes-only/){
		$unique_done = 1;
		print LOG "\n--unique-genes-only has been activated. The longest transcript is chosen if present. Otherwise, the first transcipt is chosen.\n";
		$string = "perl $loc" . "task_scripts/unique_genes.pl $ARGV[$output] $pre >> $log";

		print LOG "\tCommand: ", $string, "\n";
		print LOG "Results:\n";
		system ($string);
		$name =  $ARGV[$output] . "\/" . "\/" . $pre . "unique_genes.txt";		
		system "mv $name $gene_table";
		
		if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
			if ($INPUT =~ /--no-gene-redefining/){
	                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        	system($string);
                	        $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
        	                $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
	                }

                        print LOG "\n";
                        system ($string);
                }
	}
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
#############################################################################################################################################################
# OUTPUTS
	if ($INPUT =~ /\-\-splice\-table\s/){
                print LOG "\n--splice-table has been activated. Creating splice_table.txt catalog.";
                $string = "perl $loc" . "task_scripts/splice_table.pl $fasta $ARGV[$output] $pre >> $log";
                print LOG "\n\tCommand: ", "$string", "\n";
		print LOG "Results:\n";
                system ($string);
        }
	if($INPUT =~ /--get-fasta-with-introns/){
		print LOG "\n--get-fasta-with-introns has been activated. Nucleotide fasta will be created called genes_with_introns.fasta.";
		$string = "perl $loc" . "task_scripts/get_fasta_with_introns.pl $fasta $ARGV[$output] $pre";
		print LOG "\n\tCommand: ", $string, "\n";
		system ($string);
	}
	if ($INPUT =~ /--get-fasta/){
                print LOG "\n--get-fasta has been activated. Nucleotide fasta will be created called genes.fasta.";
                $string = "perl $loc" . "task_scripts/get_fasta_without_introns.pl $fasta $ARGV[$output] $pre";
                print LOG "\n\tCommand: ", $string, "\n";
                system ($string);
        }
	if ($INPUT =~ /--get-protein-fasta/){
		print LOG "\n--get-protein-fasta has been activated. Protein fasta will be created called genes.fasta.faa.";
                $string = "perl $loc" . "task_scripts/get_protein_fasta.pl $fasta $ARGV[$output] $pre";
                print LOG "\n\tCommand: ", $string, "\n";
                system ($string);
	}
	if ($INPUT =~ /\-\-create\-gtf|--create-simple-gtf|--create-gff3|--compatibility/){
		if ($INPUT =~ /\-\-create\-gtf/ or $INPUT =~ /--compatibility.+SnpEff/ or $INPUT =~ /--compatibility.+hisat/){
			print LOG "\n--create-gtf has been activated. A gtf file called out.gtf will be created.";
			$string = "perl $loc" . "task_scripts/add_start_stop_to_gene_table.pl $fasta $ARGV[$output] $pre";
			 print LOG "\n\tCommand: ", $string, "\n";
	                system ($string); 
	
			$start_stop_table = $ARGV[$output] . "\/" . "\/" . $pre . "start_and_stop_gene_table.txt";

	                $string = "perl $loc" . "task_scripts/gtf_creator.pl $start_stop_table $ARGV[$output] $ARGV[$infile] $pre";
        	        print LOG "\tCommand: ", $string, "\n";
                	system ($string);
			system "rm $start_stop_table";

		}
		 if ($INPUT =~ /\-\-create\-simple-gtf/){
                        print LOG "\n--create-simple-gtf has been activated. A gtf file called simple_out.gtf will be created.";
			$string = "perl $loc" . "task_scripts/gtf_creator.pl $gene_table $ARGV[$output] $gene_table $pre" . "simple";
	                print LOG "\n\tCommand: ", $string, "\n";
        	        system ($string);
                }
		 if ($INPUT =~ /\-\-create\-gff3/){
                        print LOG "\n--create-gff3 has been activated. A gff3 file called out.gff3 will be created.";
                        $string = "perl $loc" . "task_scripts/gff3_creator.pl $gene_table $ARGV[$output] $gene_table $pre ";
                        print LOG "\n\tCommand: ", $string, "\n";
                        system ($string);
                }

		if ($INPUT =~ /--compatibility/){
			print LOG "\n--compatibility has been activated! You have chosen:\n";
		
			if ($INPUT =~ /SnpEff/){
				print LOG "\nSnpEff\n\n";
               			$string = "perl $loc" . "task_scripts/compatibility/SnpEff.pl $ARGV[$output] $pre";
		                print LOG "\tCommand: ", $string, "\n";
                		system ($string);
                		print LOG "\tCreated $pre" . "snpeff_format.gff", "\n\n";
			}
			if ($INPUT =~ /EVM_1.1.1_gene_prediction/){
				print LOG "\nEVM gene prediction format (version 1.1.1) \n\n";
                                $string = "perl $loc" . "task_scripts/compatibility/EVM_1.1.1_gene_prediction.pl $ARGV[$output] $pre";
                                print LOG "\tCommand: ", $string, "\n";
                                system ($string);
                                print LOG "\tCreated $pre" . "EVM_1.1.1_gene_prediction_format.gff3", "\n\n";
			}
			if ($INPUT =~ /EVM_1.1.1_alignment/){
                                print LOG "\nEVM gene prediction format (version 1.1.1) \n\n";
				$string = "perl $loc" . "task_scripts/compatibility/EVM_1.1.1_alignment.pl $gene_table $ARGV[$output] $ARGV[$infile] $pre";
        	                print LOG "\tCommand: ", $string, "\n";
	                        system ($string);
				print LOG "\tCreated $pre" . "EVM_1.1.1_alignment_format.gff3", "\n\n";
			}
			 if ($INPUT =~ /stringtie_1.3.6_gtf/){
				 print LOG "\nStringtie gtf (version 1.3.6) \n\n";
				 $string = "perl $loc" . "task_scripts/compatibility/stringtie_1.3.6_gtf.pl $gene_table $ARGV[$output]  $ARGV[$infile] $pre";
				 print LOG "\tCommand: ", $string, "\n";
                                system ($string);
                                print LOG "\tCreated $pre" . "stringtie_1.3.6.gtf", "\n\n";
                        }
			if($INPUT =~ /hisat_2.1.0_gtf/){
				print LOG "\nHISAT gtf (version 2.1.0) \n\n";
				$string = "perl $loc" . "task_scripts/compatibility/hisat_2.1.0_gtf.pl \. $ARGV[$output] $pre" . "out.gtf $pre";
				print LOG "\tCommand: ", $string, "\n";
                                system ($string);
                                print LOG "\tCreated $pre" . "hisat_2.1.0.gtf", "\n\n";
			}
			 if($INPUT =~ /ParsEval_gff3/){
                                print LOG "\nParsEval gff3 (version ????) \n\n";
                                $string = "perl $loc" . "task_scripts/compatibility/ParsEval_gff3.pl $gene_table $ARGV[$output] $gene_table $pre ";
                                print LOG "\tCommand: ", $string, "\n";
                                system ($string);
                                print LOG "\tCreated $pre" . "ParsEval.gff3", "\n\n";
                        }

		}
	}
	if ($INPUT =~ /--nt-content/){
		print LOG "\n--nt-content has been activated. GC/AT and N content statistics on CDS sequence will be printed to the log.";
		$string = "perl $loc" . "task_scripts/GC_content_analysis.pl $fasta $ARGV[$output] $pre >> $log";
		print LOG "\n\tCommand: ", $string, "\n";
		print LOG "\nResults:\n";
		system ($string);
	}		
}
###########################################################################################################################################################
 if ($INPUT =~ /--unique-genes-only/ and $unique_done == 0){
                print LOG "\n--unique-genes-only has been activated. The longest transcript is chosen if present. Otherwise, the first transcipt is chosen.\n";
                $string = "perl $loc" . "task_scripts/unique_genes.pl $ARGV[$output] $pre >> $log";

                print LOG "\tCommand: ", $string, "\n";
                print LOG "Results:\n";
                system ($string);
                $name =  $ARGV[$output] . "\/" . "\/" . $pre . "unique_genes.txt";
                system "mv $name $gene_table";

                if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
			if ($INPUT =~ /--no-gene-redefining/){
                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
	                        system($string);
        	                $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                	        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name >> $log";
	                }

                        print LOG "\n";
                        system ($string);
               	}
        if ($INPUT =~ /\-\-false-run/){
                $string = "cp $tempgt $gene_table";
                system ($string);
        }
}
###########################################################################################################################################################
if ($INPUT =~ /\-\-statistics\s/){

	$statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table > $statistics";

	if ($INPUT =~ /--no-gene-redefining/){
                        $string = "perl $loc" . "task_scripts/gene_editor.pl $ARGV[$output] $pre";
                        system($string);
                        $output_name = $ARGV[$output] . "\/" . $pre . "new_gene_table.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $output_name > $statistics";
        }

        system ($string);

        print LOG  "\n--statistics has been activated. Statistics will be printed to statistics.txt\n";
        print LOG "\tCommand: ", $string, "\n";
}
############################################################################################################################################################
if ($INPUT =~ /\-\-distributions/){
        print LOG "\n--distributions have been activated! You have chosen:\n";

        if ($INPUT =~ /exon_lengths/){
                print LOG "\texon_lengths\n";
			if ($INPUT =~ /exon_lengths\s+(\d+)\s+/){ $step = $1;}
			else{ $step = "TBD";}
		print LOG "\tStep set to $step", "\n";
                $string = "perl $loc" . "task_scripts/distributions/exon_lengths.pl $ARGV[$output] $step $pre";
		print LOG "\tCommand: ", $string, "\n";
                system ($string);
		print LOG "\tCreated exon_lengths_distributions.tsv", "\n\n";
		undef ($step);
        }
        if ($INPUT =~ /intron_lengths/){
                print LOG "\tintron_lengths\n";
			if ($INPUT =~ /intron_lengths\s+(\d+)\s+/){ $step = $1; }
			else{ $step = "TBD";}
		print LOG "\tStep set to $step", "\n";
                $string = "perl $loc" . "task_scripts/distributions/intron_lengths.pl $ARGV[$output] $step $pre";
                print LOG "\tCommand: ", $string, "\n";
                system ($string);
                print LOG "\tCreated intron_lengths_distributions.tsv", "\n\n";
		undef ($step);
        }
	if ($INPUT =~ /CDS_lengths/){
                print LOG "\tCDS_lengths\n";
                        if ($INPUT =~ /CDS_lengths\s+(\d+)\s+/){ $step = $1; }
			else{ $step = "TBD";}
                print LOG "\tStep set to $step", "\n";
                $string = "perl $loc" . "task_scripts/distributions/CDS_lengths.pl $ARGV[$output] $step $pre";
                print LOG "\tCommand: ", $string, "\n";
                system ($string);
                print LOG "\tCreated CDS_lengths_distributions.tsv", "\n\n";
		undef ($step);
        }
	if ($INPUT =~ /gene_lengths/){
                print LOG "\tgene_lengths\n";
                        if ($INPUT =~ /gene_lengths\s+(\d+)\s+/){ $step = $1; }
			else{ $step = "TBD";}
                print LOG "\tStep set to $step", "\n";
                $string = "perl $loc" . "task_scripts/distributions/gene_lengths.pl $ARGV[$output] $step $pre";
                print LOG "\tCommand: ", $string, "\n";
                system ($string);
                print LOG "\tCreated gene_lengths_distributions.tsv", "\n\n";
		undef ($step);
        }
	if ($INPUT =~ /exon_position/){
		print LOG "\texon_position\n";
		$string = "perl $loc" . "task_scripts/distributions/exon_position.pl $ARGV[$output] $pre";
		print LOG "\tCommand: ", $string, "\n";
		system ($string);
		print LOG "\tCreated exon_position_distributions.tsv\n\n";
	}
	if ($INPUT =~ /exon_position_data/){
                print LOG "\texon_position_data\n";
                $string = "perl $loc" . "task_scripts/distributions/exon_position_verbose.pl $ARGV[$output] $pre";
                print LOG "\tCommand: ", $string, "\n";
                system ($string);
                print LOG "\tCreated data_exon_position_distributions.tsv\n\n";
        }
	 if ($INPUT =~ /intron_position/){
                print LOG "\tintron_position\n";
                $string = "perl $loc" . "task_scripts/distributions/intron_position.pl $ARGV[$output] $pre";
                print LOG "\tCommand: ", $string, "\n";
                system ($string);
                print LOG "\tCreated intron_position_distributions.tsv\n\n";
        }
	if ($INPUT =~ /intron_position_data/){
                print LOG "\tintron_position_data\n";
                $string = "perl $loc" . "task_scripts/distributions/intron_position_verbose.pl $ARGV[$output] $pre";
                print LOG "\tCommand: ", $string, "\n";
                system ($string);
                print LOG "\tCreated data_intron_position_distributions.tsv\n\n";
        }


}	
###########################################################################################################################################################

print LOG "\nCompleted! Have a great day!\n\n";

$temp = $ARGV[$output] . "\/" . "\/" . $pre . "temp_file.txt";
system "rm $temp";
        if ($INPUT =~ /\-\-false-run/){
                $string = "rm $tempgt";
                system ($string);
        }

close LOG;



###########################################################################################################################################################
###########################################################################################################################################################
###########################################################################################################################################################
# ALL gFACs scripts have a fun fact at the end as a reward for code reading. Did you know the Incas DID know about the wheel but never used it functionally?
