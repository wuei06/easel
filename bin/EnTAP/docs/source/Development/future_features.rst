Future Features / Roadmap
===============================
This page contains future features that have been requested by users with an accompanying rough roadmap for implementation. The dates outlined below are subject to change depending on resources/time. If you would like to request support for additional features please email Alexander Hart at entap.dev@gmail.com

Middle 2021
-----------------------
    * Galaxy integration
    * Support for Docker packaging
    * Flask application

Future Release
--------------------
    * Support for annotation of non-coding RNA
    * Alternative to RSEM expression analysis
    * XML formatted output
