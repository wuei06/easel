import argparse
import os
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq

parser = argparse.ArgumentParser(
     prog='createGFF.py',
     usage='''python get_true_start_sites.py  --gff [gff file given by braker] --path [Path of gff file] --fasta [name of list file] --out [name of output fasta file]''',
     description='''This program pulls out the gff of specific genes, given the gff file and a list of sequences saved in a text file''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--gff', type=str, help='The name of the gff file', required=True)
parser.add_argument('--path', type=str, help='The path of the gff file', required=False)
parser.add_argument('--fasta', type=str, help='name of the list file that contains name of sequences', required=True)
parser.add_argument('--pathList', type=str, help='path of list file', required=False)
parser.add_argument('--out', type=str, help='name of output fasta file', required=True)

args=parser.parse_args()
path=args.path
gff=args.gff
output=args.out
fastafile=args.fasta
inputGFF=open(gff, "r")
print(inputGFF)

tss={}

id_dict=SeqIO.to_dict(SeqIO.parse(fastafile, "fasta"))
identifiers = [id_dict[seq_record].id for seq_record in id_dict]

for identifier in identifiers:
    tss[identifier]=[]

gffline = inputGFF.read()
blocks = gffline.split("###") 

for block in blocks:
    if "biotype=protein_coding" in block:
            transcripts = block[1:].split("mRNA")
            for transcript in transcripts[1:]:
                lines=transcript.split("\n")
                #print(lines[1])
                if "\t-\t" in lines[0]:
                    print('reverse')
                    for line in reversed(lines):
                        if "\tCDS\t" in line and "MtDNA" not in line:
                            cols=line.split("\t")
                            id_tag=cols[8].split(";")[1]
                            tss[cols[0]].append((cols[4],cols[6],id_tag))
                            break
                else:
                    print('forward')
                    for line in lines:
                        if "\tCDS\t" in line and "MtDNA" not in line:
                            cols=line.split("\t")
                            id_tag=cols[8].split(";")[1]
                            tss[cols[0]].append((cols[3],cols[6],id_tag))
                            break
    else:
        pass

done=[]
with open (output, 'w') as o:
    for scaff in tss.keys():
        for val in tss.get(scaff):
            info=(scaff, val[0], val[1])
            if info not in done:
                o.write(scaff+"\t"+val[0]+"\t"+val[1]+"\t"+val[2]+"\n")
                done.append(info)
            else:
                pass
o.close()
