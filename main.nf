def helpMessage() {
	log.info"""
	========================================================================================
                 EASEL - Efficient, Accurate Scalable Eukaryotic modeLs
	========================================================================================
 	
	Usage:
	nextflow main.nf --genome /path/to/genome.fa --orthoDB /path/to/database --sra /path/to/sralist.txt [...] 
	or
	nextflow main.nf -params-file params.yaml
	
	Required arguments:
		--genome				Path to masked genome (*.fa/*.fasta)
		--sra					Path to SRA accession list
		  or
		--user_reads			Path to user RNA reads (*_{1,2}.fastq)
		--orthoDB				Relevant OrthoDB protein database as source of reference protein sequence (example: embryophyta)
		--taxon					Relevant taxon for functional annotation

	Recommended arguments:
		--outdir				 Path to the output directory (default: easel_taxon)
		--prefix				 AUGUSTUS training identifier (default: taxon)
		--max_memory             Maximum memory allocated
    	--max_cpus               Maximum cpus allocated
    	--max_time               Maximum time allocated

	Optional arguments:
		--bins					Number of bins genome will be split into (default: 50)
		--rate					Filtering mapping rate cut-off (default: 85)
		--total_reads			Filtering read length cut-off (default: 15000000)
		--mean_length			Filtering mean length of reads cut-off (default: 70)
		--cluster_id			USEARCH identify threshold (default: 0.80)
		--aligner				Protein alignment tool [gth/miniprot] (default: gth)
		--N					    Miniprot: retain at most NUM number of secondary chains/alignments (default: 30)
		--outn					Miniprot: output up to min{NUM, -N} alignments per query (default: 1000)
		--max_intron			Miniprot: max intron size (default: 200000)
		--test					Number of genes to test AUGUSTUS (default: 250)
		--train					Number of genes to train AUGUSTUS (default: 1000)
		--tcoverage				EnTAP t-coverage cutoff (default: 50)
		--qcoverave				EnTAP q-coverage cutoff (default: 50)
		--window				RNA folding window size, upstream_downstream (default: 400_200)
		--external_protein		If true, AUGUSTUS will be run with orthoDB proteins (default: false)

	Resuming nextflow:
		-resume					Resume script from last step 
}
	
	Documentation:
	https://gitlab.com/PlantGenomicsLab/easel-augustus-training

 	Authors:
	Cynthia Webster <cynthia.webster@uconn.edu>
	========================================================================================

    """.stripIndent()
}

params.help = false
if (params.help){
    helpMessage()
    exit 0
}

nextflow.enable.dsl=2

include { main_workflow as MAIN } from './workflows/main_workflow.nf'

workflow {

    MAIN()

    }
